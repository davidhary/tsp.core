## ISR Tsp Tsp<sub>&trade;</sub>: ISR Test Script Processor Core Framework.
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*4.0.5242 05/09/14*<br/>	Adds support for TSP 2.0. Adds display text and input.
Adds device with IEEE 488 instrument properties.

*4.0.5237 05/04/14*<br/>	Upgrades to using extended string, user string, array,
table, math and type functions.

*4.0.5232 04/30/14*<br/>	Removes Commit history. Uses MIT license. Adds spaces
to comments. Removes new lines in comments.\
Created based on the legacy support project aiming to reduce the
footprint of the TTM package by removing the KTS code.

\(C\) 2005 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Eclipse ® from the
[Eclipse Foundation](http://www.eclipse.org).

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:[\
Lua Core Project](https://bitbucket.org/davidhary/tsp.core)[\
LUA](http://www.lua.org)[\
table.copy](http://www.loop.org)[\
Working LUA base-64 codec](http://www.it-rfc.de)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:[\
Test Script Builder](http://www.keithley.com)[\
LUA](https://www.lua.org)
