--- <summary> Supports Enum. </summary>
--- <namespace> _G.isr </namespace>
--- <license> (c) 2014 Integrated Scientific Resources, Inc.<para>
--- Licensed under The MIT License. </para><para>
--- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
--- BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
--- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--- </para> </license>
--- <history date='05/10/14' by='David Hary' revision='4.0.5243.x'> Created. </history>

do

  local _chunkName = 'isr.enum.tsp'
  if _G.enum == nil then error('enum support is required') end

  --- <summary> Gets or sets a namespace for ISR code. </summary>
  isr = isr or {}

  --- <summary> Creates a copy of the table. </summary>
  --- <param name='lTable'> The table. </param>
  local _copy = function(lTable) 
    local lNewTable = {}
    for lKey, lValue in pairs(lTable) do
        lNewTable[lKey] = lValue
    end
    return lNewTable
  end

  --- <summary> Creates a copy of the system enum. </summary>
  local _enum = _copy(_G.enum)
   
  --- <summary> Gets or sets a namespace for ISR enum commands. </summary>
  _G.isr.enum = _enum

  --- <summary> Checks if the value is enum type. </summary>
  --- <param name='value'> The value. </param>
  --- <returns> <c>true</> if enum. <returns>
  _enum.isEnum = function(value)
    -- nil enum number conversion returns the same value as non-enum userdata
    return value ~= nil and 'userdata' == type(value) and 
           _enum.tonumber(value) ~= _enum.tonumber(nil)
  end

  --- <summary> Tries to convert the value to a number; error if not enum or number. </summary>
  --- <param name='value'> The value. </param>
  --- <example> <para>
  --- local _enum = isr.enum
  --- a = _enum.new(1,'a', 5)
  ---                                   </para><para> 
  --- print(_G.enum.tonumber(a)) -- 5
  ---                                   </para><para> 
  --- print(_enum.tryConvertNumber(a))    -- true, 5
  ---                                   </para><para> 
  --- print(_enum.tryConvertNumber('5'))  -- false '5'
  ---                                   </para><para> 
  --- print(_enum.tryConvertNumber(nil))  -- false nil
  --- </para> </example>
  --- <returns> true, number or false, value if value is not number or enum. </returns>  
  local _tryConvertNumber = function(value)
    -- enum is typed as user data.
    if _enum.isEnum then
      return true,_enum.tonumber(value)
    elseif 'number' == type(value) then
      return true,value
    else 
      return false,value
    end
  end

  --- <summary> Converts the value to a number; error if not enum or number. </summary>
  --- <param name='value'> The value. </param>
  --- <example> <para>
  --- local _enum = isr.enum
  --- a = _enum.new(1,'a', 5)
  ---                                   </para><para> 
  --- print(_enum.toNumber(a))    -- 5
  ---                                   </para><para> 
  --- print(_enum.toNumber('5'))  -- error
  --- </para> </example>
  --- <returns> A number; error if not number or enum. </returns>  
  --- <namespace> _G.isr.enum </namespace>
  local _toNumber = function(value)
    local isNumber = false
    -- enum is types as user data.
    isNumber, value = _tryConvertNumber(value) 
    if not isNumber then
      if nil == value then
        error('argument exception: invalid data type value=nil; expected number or enum') 
      else
        error('argument exception: invalid data type value='..type(value).."; expected number or enum") 
      end
    end
    return value
  end

  --- <summary> Converts the pair to numbers; error if not enum or number. </summary>
  --- <param name='left'> The left hand value. </param>
  --- <param name='right'> The right hand value. </param>
  --- <example> <para>
  --- local _enum = isr.enum
  --- a = _enum.new(1,'a', 5)
  ---                                   </para><para> 
  --- b = _enum.new(1,'a', 9)
  ---                                   </para><para> 
  --- print(_enum.toNumberPair(a, b))    -- 5 9
  ---                                   </para><para> 
  --- print(_enum.toNumberPair(a, '5'))  -- error
  --- </para> </example>
  --- <returns> A number pair; error if not number or enum. </returns>  
  --- <namespace> _G.isr.enum </namespace>
  local _toNumberPair = function(left, right)

    local isNumber = false
    
    -- enum is types as user data.
    isNumber, left = _tryConvertNumber(left) 
    if not isNumber then 
      if nil == left then
        error('argument exception: invalid data type left=nil; expected number or enum') 
      else
        error('argument exception: invalid data type left='..type(left).."; expected number or enum") 
      end
    end

    -- enum is types as user data.
    isNumber, right = _tryConvertNumber(right) 
    if not isNumber then 
      if nil == right then
        error('argument exception: invalid data type right=nil; expected number or enum') 
      else
        error('argument exception: invalid data type right='..type(right).."; expected number or enum") 
      end
    end

    return left,right
  end

  --- <summary> Implements bitwise AND. </summary>
  --- <param name='left'> The left hand value. </param>
  --- <param name='right'> The right hand value. </param>
  --- <example> <para>
  --- local _enum = isr.enum
  --- a = _enum.new(1,'a', 5)
  ---                                   </para><para> 
  --- b = _enum.new(1,'a', 9)
  ---                                   </para><para> 
  --- print(a + b) -- 14 
  ---                                   </para><para> 
  --- print(_enum.bitand(a, b)) -- 1
  ---                                   </para><para> 
  --- print(_enum.bitor(a, b))  -- 13
  ---                                   </para><para> 
  --- print(_enum.bitor(a, '4'))-- error
  ---                                   </para><para> 
  --- </para> </example>
  --- <returns> left AND right. </returns>  
  --- <namespace> _G.isr.enum </namespace>
  _enum.bitand = function(left, right)
    return bit.bitand(_toNumberPair(left, right))
  end

  --- <summary> Implements bitwise OR. </summary>
  --- <param name='left'> The left hand value. </param>
  --- <param name='right'> The right hand value. </param>
  --- <example> <para>
  --- local _enum = isr.enum
  ---                                   </para><para> 
  --- a = _enum.new(1,'a', 5)
  ---                                   </para><para> 
  --- print(_enum.isEnum(a)) -- true
  ---                                   </para><para> 
  --- b = _enum.new(1,'a', 9)
  ---                                   </para><para> 
  --- print(a + b) -- 14 
  ---                                   </para><para> 
  --- print(_enum.bitand(a, b)) -- 1
  ---                                   </para><para> 
  --- print(_enum.bitor(a, b))  -- 13
  ---                                   </para><para> 
  --- print(_enum.bitor(a, '5'))-- error
  ---                                   </para><para> 
  --- print(_enum.bitor(a, nil))-- error
  ---                                   </para><para> 
  --- </para> </example>
  --- <returns> left AND right. </returns>  
  --- <namespace> _G.isr.enum </namespace>
  _enum.bitor = function(left, right)
    return bit.bitor(_toNumberPair(left, right))
  end

  -- Store chunk name at a standard location for require() to look it up.
  _G._LOADED[_chunkName] = _enum

end
