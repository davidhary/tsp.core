--- <summary> Defines a reusable construct for inheriting Lua classes.
--- Each class can inherit from the super class. </summary>
--- <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
--- Licensed under The MIT License. </para><para>
--- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
--- BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
--- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--- </para> </license>
--- <history date='03/22/05' by='David Hary' revision='1.0.1907.x'> Created </history>

do

  local _chunkName = 'baseClass.lua'

  --- <summary> The base object class. </summary>
  local baseObject = {
    super  = nil,
    name  = 'Object',
    new   = function(class)
      local obj = {class = class}
      -- fix 04/30/2014
      local meta = {__index = function(self, key) return self.class.methods[key] end}
      -- local meta = {__index = function(self, key) return class.methods[key] end}
      setmetatable(obj, meta)
      return obj
    end,
    methods = {className = function(self) return self.class.name end},
    data = {}
    }

  --- <summary> Clones the base object class to create new classes. </summary>
  --- <param name='name'> The name of the new class. </param>
  --- <param name='super'> The parent or super class from which
  ---   this class inherits. </param>
  --- <returns> A class that inherit from the base class object or from
  ---   the super class. </returns>
  --- <remarks> This class cloning function executes the class init method
  ---   when the class is instantiated. </remarks>
  --- <example> <para><p>
  --- see base class tester under the testers folder.
  --- </para> </example>
  local baseClass = function(name, super)

    if nil == super then
      super = BaseObject
    end

    local class = {
      super = super,
      name = name, 
      new  = function(self,...) 
        local obj = super.new(self, '___CREATE_ONLY___')
        -- check if calling function init
        -- pass arguments into init function
        if (super.methods.init) then obj.init_super = super.methods.init end
        if (tostring(arg[1]) ~= '___CREATE_ONLY___') then
          obj.init = self.methods.init
          obj:init(unpack(arg))
        end
        return obj
      end,
      methods = {}
    }

    -- if class slot unavailable, check super class
    -- if applied to argument, pass it to the class method new
    setmetatable(class,
                 {__index = function(self,key) return self.super[key] end,
                  __call  = function(self,...) return self.new(self, unpack(arg)) end
                  }
                )

    -- if instance method unavailable, check method slot in super class
    setmetatable(class.methods,
       -- {__index = function(self,key) return class.super.methods[key] end 
          {__index = function(self,key) return self.class.super.methods[key] end 
          }
      )
   
    return class

  end

  -- Store chunk name at a standard location for require() to look it up.
  _G._LOADED[_chunkName] = baseClass

end
