--- <summary> XOR-based cipher. </summary>
--- <namespace> _G </namespace>
--- <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
--- Licensed under The MIT License. </para><para>
--- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
--- BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
--- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--- </para> </license>
--- <history date='03/21/09' by='David Hary' revision='2.1.2657.x'> Created </history>
--- <tested date='09/11/14' by='David Hary' revision='4.0.5367.x' > 2601B, 2450 </history>

do

  local _chunkName = 'xor.cipher.lua'
  local _require = require 'require.lua'
  local _base64 = _require('base64.lua', _chunkName)

  local _xor = {}
  
  --- <summary> Encodes the given value. </summary>
  --- <param name='value'> Value to encode. </param>
  --- <returns> Encoded string in base 64. </returns>
  --- <example> <para>
  ---                                                   </para><para>
  --- _require = require 'require.lua'
  ---                                                   </para><para>
  --- _xor = _require('xor.cipher.lua','<this chunk>')
  ---                                                   </para><para>
  --- print(_xor.decode(_xor.encode('abc', '123'), '123')) -- abc
  ---                                                   </para><para>
  --- print(_xor.decode(_xor.encode('AAABBBccc', 'A123'), 'A123')) -- AAABBBccc
  ---                                                   </para><para>
  --- print(_xor.decode(_xor.encode('Abc', 'A123'), 'A123')) -- Abc
  ---                                                   </para><para>
  --- print(_xor.decode(_xor.encode('AAc', 'A123'), 'A123')) -- AAc
  ---                                                   </para><para>
  --- print(_xor.decode(_xor.encode('Aabb', 'A11'), 'A11')) -- Aabb
  ---                                                   </para><para>
  --- </para> </example>
  --- <namespace> _G </namespace>
  _xor.encode = function(value, key) 
  
    local xor = bit.bitxor
    local tochar = string.char
    local tobyte = string.byte

    local encoded = ''

    local n = string.len(value)
    local m = string.len(key)

    -- convert the sting to a 8bit binary value
    local j = 1
    for  i = 1, n, 1 do
    
      -- must add one because base 64 does not accept a null (zero) string
      ascii = 1 + xor(tobyte(value, i), tobyte(key, j)) 
      encoded = encoded .. tochar(ascii)
      j = j + 1
      if j > m then
        j = 1
      end

    end
    
    return _base64.toBase64(encoded) 

  end
  
  --- <summary> Encodes the given value. </summary>
  --- <param name='value'> Value to encode. </param>
  --- <returns> Encoded string in base 64. </returns>
  --- <example> <para>
  ---                                                   </para><para>
  --- _require = require 'require.lua'
  ---                                                   </para><para>
  --- _xor = _require('xor.cipher.lua','<this chunk>')
  ---                                                   </para><para>
  --- print(_xor.decode(_xor.encode('abc', '123'), '123')) -- abc
  ---                                                   </para><para>
  --- </para> </example>
  --- <namespace> _G </namespace>
  function _xor.decode(value, key) 
  
    local xor = bit.bitxor
    local tochar = string.char
    local tobyte = string.byte

    local encoded = _base64.fromBase64(value) 
    local decoded = ''

    local n = string.len(encoded)
    local m = string.len(key)

    -- convert the sting to a 8bit binary value
    local j = 1
    for  i = 1, n, 1 do
    
      -- the encoding adds a 1
      ascii = tobyte(encoded, i)  - 1
      decoded = decoded .. tochar(xor(ascii, tobyte(key, j)))
      j = j + 1
      if j > m then
        j = 1
      end

    end
    
    return decoded

  end
  
  -- Store chunk name at a standard location for require() to look it up.
  _G._LOADED[_chunkName] = _xor

end
