--- <summary> Adds math functions. </summary>
--- <namespace> _G </namespace>
--- <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
--- Licensed under The MIT License. </para><para>
--- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
--- BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
--- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--- </para> </license>
--- <history date='05/23/05' by='David Hary' revision='1.0.1969.x'> Created </history>

do

  local _chunkName = 'g.math.lua'

  --- <summary> Computes the rounded up value based on the provided resolution. </summary>
  --- <param name='value'> Value to round up. </param>
  --- <param name='resolution'> Resolution by which to round up. </param>
  --- <returns> resolution * _G.math.ceil(value / resolution) </returns>
  --- <example> <para>
  --- print(string.format('ceiling(%7.4f, %7.4f) = %7.4f', math.pi, 0.01, math.ceiling(math.pi, 0.01)))
  --- print(string.format('ceiling(%7.4f, %7.4f) = %7.4f', math.pi, 0.001, math.ceiling(math.pi, 0.001)))
  --- </para> </example>
  math.ceiling = function(value, resolution)

    return  resolution * math.ceil(value / resolution)

  end

  --- <summary> Clips the value if below or above the limits. </summary>
  --- <param name='value'> Value to clip. </param>
  --- <param name='min'> The minimum. </param>
  --- <param name='max'> The maximum. </param>
  --- <returns> The value  or min if lower than min or max if higher than max. </returns>
  --- <example> <para>
  --- print(math.clip(1, -10, 10))   -- 1
  --- print(math.clip(11, -10, 10))  -- 10
  --- print(math.clip(-11, -10, 10)) -- -10
  --- </para> </example>
  math.clip = function(value, min, max)
      
      return math.min(math.max(value, min), max)
  
  end
  
  -- Store chunk name at a standard location for require() to look it up.
  _G._LOADED[_chunkName] = math

end
