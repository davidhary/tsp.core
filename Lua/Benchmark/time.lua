_G._LOADED['time'] = function(title, f)

  collectgarbage()

  local startTime = os.clock()

  for i=0,1000 do f() end

  local endTime = os.clock()

  print(string.format('%s %7.4f ms', title, endTime - startTime ))

end


