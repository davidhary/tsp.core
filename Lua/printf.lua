--- <summary> Formatted print. </summary>
--- <namespace> _G </namespace>
--- <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
--- Licensed under The MIT License. </para><para>
--- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
--- BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
--- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--- </para> </license>
--- <history date='09/10/09' by='David Hary' revision='2.2.3540.x'> Add print integer format. </history>
--- <history date='03/28/07' by='David Hary' revision='1.0.2643.x'> Created </history>

do

  local _chunkName = 'g.printf.lua'

  --- <summary> Outputs data using C formatting. </summary>
  --- <example> <para>
  --- printf('Hello from %s on %s\n', _VERSION, os.date())
  --- Hello from Lua 5.0.2 on Wed May  7 16:46:08 2014
  --- </para> </example>
  function printf(...)
    print(string.format(unpack(arg)))
  end

  --- <summary> Outputs data using integer format. </summary>
  --- <example> <para>
  --- printi(1, 3)
  --- </para> </example>
  function printi(...)
    if arg then
      local n = 0 
      for _, _ in pairs(arg) do 
        n = n + 1 
      end
      -- local n = table.getn(arg) - 1
      if n == 1 then
        print(string.format('%d', unpack(arg)))
      else
        print(string.format(string.rep('%d ', n - 1), unpack(arg)))
      end
   end
   
  end

  -- Store chunk name at a standard location for require() to look it up.
  _LOADED[_chunkName] = true

end
