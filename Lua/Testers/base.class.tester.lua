
do

  require('Animal Tiger.lua', 'basecallstater')
  ---
  print('Zoo Animal.')
  zooAnimal = Animal:new('Hunt', 'Zoo Animal')
  print('Class name:', zooAnimal:className())
  print('Super action:',zooAnimal.superAction)
  print('Super Family:',zooAnimal.superFamily)
  print('Action:     ',zooAnimal.action)
  print('Family:     ',zooAnimal.family)
  print('===============')
  print('Bengal Tiger')
  bengalTiger = Tiger:new('Bengal Tiger')
  print('Class name:', bengalTiger:className()) 
  print('Super action:',bengalTiger.superAction)
  print('Super Family:',bengalTiger.superFamily)
  print('Action:     ',bengalTiger.action)
  print('Family      ',bengalTiger.family)
  print('===============')
  print('Mountain Lion.')
  mountainLion = Tiger:new('Mountain Lion')
  print('Class name: ', mountainLion:className()) 
  print('Super action:',mountainLion.superAction)
  print('Super Family:',mountainLion.superFamily)
  print('Action:     ',mountainLion.action)
  print('Family:     ',mountainLion.family)

  ---   Animal = _G.isr.baseClass('Animal') </p><p>
  ---   function Animal.methods:init(action, family)  </p><p>
  ---     self.superAction = action </p><p>
  ---     self.superFamily = family </p><p>
  ---   end </p><p>
  ---   Tiger = _G.isr.baseClass('Tiger', Animal) </p><p>
  ---   function Tiger.methods:init(family)  </p><p>
  ---     self:init_super('Hunt (Tiger)', 'Zoo Animal (Tiger)') </p><p>
  ---     self.action = 'Roar' </p><p>
  ---     self.family = family </p><p>
  ---   end </p><p>
  ---   print('Zoo Animal.')    </p><p>
  ---   zooAnimal = Animal:new('Hunt', 'Zoo Animal') </p><p>
  ---   print('Class name:', zooAnimal:className())  </p><p>
  ---   print('Super action:',zooAnimal.superAction) </p><p>
  ---   print('Super Family:',zooAnimal.superFamily) </p><p>
  ---   print('Action:     ',zooAnimal.action) </p><p>
  ---   print('Family:     ',zooAnimal.family) </p><p>
  ---   print('===============') </p><p>
  ---   print('Bengal Tiger')    </p><p>
  ---   bengalTiger = Tiger:new('Bengal Tiger') </p><p>
  ---   print('Class name: ', bengalTiger:className())  </p><p>
  ---   print('Super action:',bengalTiger.superAction) </p><p>
  ---   print('Super Family:',bengalTiger.superFamily) </p><p>
  ---   print('Action:       ',bengalTiger.action) </p><p>
  ---   print('Family:       ',bengalTiger.family) </p>

end



