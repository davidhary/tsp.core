
do

  local ns = _G.isr.array
  local a = {}

  function linearArrayTest()
    a = ns.createLinearArray(1, 1, 3)
    print(string.format('length: %d', ns.length(a)))
    ns.printArray(a)
  end

  function linearScaleTest()
    local slope = 2
    local offset = 0.5
    printf('linear scale: Offset=%f slope=%f', offset, slope)
    ns.printTwoArraysIndexValues(a, ns.linearScale(a, 2, 0.5))
  end

  function offsetTest()
    a = ns.createLinearArray(-1, -1, 10)
    print(string.format('offset: %d', ns.findOffset(a, -5)))
    ns.printArray(a)
  end

  function onsetTest()
    a = ns.createLinearArray(1, 1, 10)
    print(string.format('onset: %d', ns.findOnset(a, 5)))
    ns.printArray(a)
  end

  function limenTest()
    local a = ns.createLinearArray(1, 1, 5)
    local b = ns.createLinearArray(6, -1, 5)
    local c = ns.concatenate(a, b)
    local limen = 5
    local hystheresis = 2
    local minimumCount = 3
    print(string.format('onset (%d,%d,%d): %d', limen, hystheresis, minimumCount, ns.findLimen(c, limen, hystheresis, minimumCount)))
    limen = 4
    print(string.format('onset (%d,%d,%d): %d', limen, hystheresis, minimumCount, ns.findLimen(c, limen, hystheresis, minimumCount)))
    ns.printArray(c)
    c = ns.concatenate(b, a)
    limen = 1
    hystheresis = -2
    minimumCount = 3
    print(string.format('offset (%d,%d,%d): %d', limen, hystheresis, minimumCount, ns.findLimen(c, limen, hystheresis, minimumCount)))
    limen = 2
    print(string.format('offset (%d,%d,%d): %d', limen, hystheresis, minimumCount, ns.findLimen(c, limen, hystheresis, minimumCount)))
    ns.printArray(c)
  end

  function arrayCopyTest()
    print(' copy:')
    ns.printTwoArraysIndexValues(a, ns.copy(a))
  end


  function medianFilterTest() 
    _G.math.randomseed(os.clock())
    a = ns.createRandomIntegerArray(10, 1, 20)
    b = ns.medianFilter(a, 5) 
    ns.printTwoArrays(a, b)
  end

  function arrayInterpolateTest()
    print(' interpolate:')
    local b = ns.createLinearArray(0, 0.5, 5)
    ns.printTwoArrays(b, ns.linearInterpolate(b, a, a))
  end

  function linearFitTest()
    local offset, slope = ns.linearFit(a, ns.linearScale(a, 2, 0.5))
    print(string.format(' offset: %f, slope: %f', offset, slope))
  end

  function arrayProductTest()
    print(' product:')
    ns.printArrayIndexValue(ns.product(a, a))
  end

  function arrayShiftTest()
    print(' shift:')
    ns.printArrayIndexValue(ns.shift(a, 1))
  end

  function arraySumTest()
    print(' sum:', ns.sum(a, 1))
  end

end
