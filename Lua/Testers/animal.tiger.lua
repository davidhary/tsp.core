--- <module id='Animal Tiger' namespace='isr.Testers' assembly='isr.Testers'
---      date='03/22/05' author='David Hary' 
---      copyright='Copyright � 2005-2009 by Integrated Scientific Resources, Inc.'>
--- <summary>Includes classes and methods for testing the implementation
---   of classes and inheritance in Lua..</summary>
--- <remarks> </remarks>
--- <history date='03/22/05' by='David Hary' revision='1.0.1907.x'>
---   Create
--- </history>
--- </class>
--- -----------------------------------------------------------------------------
-- require('base class.lua')

--- <class id='Animal' namespace='isr.Lua.Core.Testers' assembly='isr.Lua.Core.Testers'
---      date='03/22/05' author='David Hary' 
---      copyright='Copyright � 2005-2009 by Integrated Scientific Resources, Inc.'>
--- <summary>Defines a parent animal class.</summary>
--- <remarks> </remarks>
--- <history date='03/22/05' by='David Hary' revision='1.0.1907.x'>
---   Create
--- </history>
--- </class>
--- -----------------------------------------------------------------------------
Animal = _G.isr.baseClass('Animal')

--- <summary>Initializes this class whenever the class is instantiated.</summary>
--- <param name='action'>Specifies the animal action name.</param>
--- <param name='family'>Specifies a family name for the animal
---   such as 'Zoo Animal'.</param>
--- <remarks> <remarks> 
--- -----------------------------------------------------------------------------------
function Animal.methods:init(action, family) 
  self.superAction = action
  self.superFamily = family
end

--- <class id='Tiger' namespace='isr.Lua.Core.Testers' assembly='isr.Lua.Core.Testers'
---      date='03/22/05' author='David Hary' 
---      copyright='Copyright � 2005-2009 by Integrated Scientific Resources, Inc.'>
--- <summary>Defines a Tiger class that inherits from Animal.</summary>
--- <remarks> <remarks> 
--- <history date='03/22/05' by='David Hary' revision='1.0.1907.x'>
---   Create
--- </history>
--- </class>
--- -----------------------------------------------------------------------------
Tiger = _G.isr.baseClass('Tiger', Animal)

--- <summary>Initializes this class whenever the class is instantiated.</summary>
--- <param name='action'>Specifies the animal action name.</param>
--- <param name='family'>Specifies a family name for the animal
---   such as 'Zoo Animal'.</param>
--- <remarks> <remarks> 
--- -----------------------------------------------------------------------------------
function Tiger.methods:init(family) 
	self:init_super('Hunt (Tiger)', 'Zoo Animal (Tiger)')
	self.action = 'Roar'
	self.family = family
end

if false then
end
