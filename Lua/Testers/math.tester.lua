
do

  ns = _G.isr.math

  --- <summary> Tests rounding up. </summary>
  --- <remarks> </remarks>
  function testRoundUp()
    local num = 1.234
    local res = 0.01
    print(string.format('round up %7.4f res. %7.4f = %7.4f', num, res, ns.roundUp(num, res)))
  end

end
