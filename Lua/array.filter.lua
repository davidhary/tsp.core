--- <summary> Array filter functions. </summary>
--- <remarks> The array functions were written so as to work with TSP buffers. </remarks>
--- <namespace> isr.array </namespace>
--- <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
--- Licensed under The MIT License. </para><para>
--- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
--- BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
--- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--- </para> </license>
--- <history date='03/24/05' by='David Hary' revision='1.0.1909.x'> Created </history>

do

  local _chunkName = 'isr.array.filter.lua'

  -- aliases
  local _require = require 'require.lua'
  _require('isr.array.lua', _chunkName)

  --- <summary> Defines the ISR name space. </summary>
  isr = isr or {}

  --- <summary> Defines the ISR array name space. </summary>
  isr.array = isr.array or {}

  local Public = isr.array
  -- Get local access to the global environment
  local _G = _G
  -- Declare all public functions as global variables and they will go to 
  -- a separate table automatically. Register this table as the package name. 
  setfenv(1, Public)

  --- <summary> Returns the median of the input array. </summary>
  --- <param name='a'> The input array. </param>
  --- <param name='from'> The starting index (1). </param>
  --- <param name='to'> The ending index (length). </param>
  --- <returns> median point. </returns>
  --- <example> <para>
  ---                                   </para><para> 
  --- print(isr.array.median({1,4,2,5},2,4)) -- 4
  ---                                   </para><para> 
  --- </para> </example>
  --- <returns> median of the array. </returns> 
  function median(a, from, to)

    local median 
    local size = to - from + 1

    -- check the order
    if 0 > size then

      _G.error(string.format('Bad median size: %d=%d-%d.', size, to, from))

    elseif _G.math.mod(size, 2) ~= 1 then

      _G.error(string.format('Size must be odd: %d=%d-%d.', size, to, from))

    else

      local newArray = copy(a, from, to)
      sort(newArray)
      median = newArray[(size + 1) / 2]

    end

    return median

  end

  --- <summary> Returns a filtered array. </summary>
  --- <param name='a'> The array to filter. </param>
  --- <param name='size'> The size of the filter (3). Must be odd. </param>
  --- <param name='from'> The starting index (1). </param>
  --- <param name='to'> The ending index (length). </param>
  --- <remarks> End points are filtered assuming the array is monotonically
  ---   increasing. </remarks>
  --- <example> <para>
  --- a = isr.array.createRandomIntegerArray(10, 1, 20)
  ---                                   </para><para> 
  --- b = isr.array.medianFilter(a, 5) 
  ---                                   </para><para> 
  --- isr.array.printTwoArrays(a, b)
  ---                                   </para><para> 
  --- </para> </example>
  --- <returns> Array. </returns> 
  function medianFilter(a, size, from, to)

    local i, j = from or 1, to or getn(a)
    if nil == size then
      size = 3
    elseif 3 > size then
      _G.error(string.format('Bad median filter size of %d. Size must be greater than 2.', size))
    elseif _G.math.mod(size, 2) ~= 1 then
      _G.error(string.format('Bad median filter size=%d.  Must be odd.', size))
    end

    local newArray = {}
    setn(newArray, j - i + 1)
    local halfWindow = (size - 1) / 2
    for k = i, i + halfWindow - 1 do
      local currentOffset = k - _G.math.max(1, k - halfWindow) 
      newArray[k] = median(a, k - currentOffset, k + currentOffset)
    end
    for k = i + halfWindow, j - halfWindow - 1 do
      newArray[k] = median(a, k - halfWindow, k + halfWindow)
    end
    for k = j - halfWindow, j do
      local currentOffset = _G.math.min(j, k + halfWindow) - k
      newArray[k] = median(a, k - currentOffset, k + currentOffset)
    end

    return newArray

  end

  -- return was commented out and the reset of the environment was added to fix
  -- the problem of packages not loading in a configuration sequence.
  _G.setfenv(1, _G)

  -- Store chunk name at a standard location for require() to look it up.
  _G._LOADED[_chunkName] = isr.array

end
