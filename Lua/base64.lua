--- <summary> Supports base 64 conversions. </summary>
--- <namespace> _G </namespace>
--- <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
--- Licensed under The MIT License. </para><para>
--- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
--- BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
--- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. </para> </license>
--- <history date='03/21/09' by='David Hary' revision='2.1.3367.x'> Based on the 
--- 'working Lua base64 codec' by Alex Kloss; http://www.it-rfc.de </history>
--- <tested date='09/11/14' by='David Hary' revision='4.0.5367.x' > 2601B, 2450 </history>

do

  local _chunkName = 'base64.lua'
  local _require = require 'require.lua'
  local _bitwise = _require('g.bitwise.lua', _chunkName)
  
  local _base64 = {}
  
  --- <summary> Encryption table. Uses URL Safe characters. </summary>
  local _chars = {[ 0]='A',[ 1]='B',[ 2]='C',[ 3]='D',[ 4]='E',[ 5]='F',[ 6]='G',[ 7]='H',[ 8]='I',[ 9]='J',
                  [10]='K',[11]='L',[12]='M',[13]='N',[14]='O',[15]='P',[16]='Q',[17]='R',[18]='S',[19]='T',
                  [20]='U',[21]='V',[22]='W',[23]='X',[24]='Y',[25]='Z',[26]='a',[27]='b',[28]='c',[29]='d',
                  [30]='e',[31]='f',[32]='g',[33]='h',[34]='i',[35]='j',[36]='k',[37]='l',[38]='m',[39]='n',
                  [40]='o',[41]='p',[42]='q',[43]='r',[44]='s',[45]='t',[46]='u',[47]='v',[48]='w',[49]='x',
                  [50]='y',[51]='z',[52]='0',[53]='1',[54]='2',[55]='3',[56]='4',[57]='5',[58]='6',[59]='7',
                  [60]='8',[61]='9',[62]='-',[63]='_'}

  --- <summary> Encodes input string to base64. 
  --- The input string must not include null characters. </summary>
  --- <param name='value'> The string. </param>
  --- <example> <para>
  ---                                                   </para><para>
  --- _require = require 'require.lua'
  ---                                                   </para><para>
  --- _base64 = _require('base64.lua','<this chunk>')
  ---                                                   </para><para>
  --- print(_base64.fromBase64(_base64.toBase64('David'))) -- David
  ---                                                   </para><para>
  --- </para> </example>
  --- <namespace> _G </namespace>
  _base64.toBase64 = function(value)
  
    local lsh = bitwise.lsh
    local rsh = bitwise.rsh
    local lor = bit.bitor
    local mod = math.mod
    
    local bytes = {}
    local n = string.len(value)
    local result = ''
    
    for  i = 0, n - 1, 3 do
      
      for j = 1, 3 do 
        -- bytes[j] = string.byte(string.sub(value, i + j)) or 0 
        bytes[j] = string.byte(value, i + j) or 0 
      end
      
      result = string.format('%s%s%s%s%s', result, 
                    _chars[rsh(bytes[1], 2)], 
                    _chars[lor(lsh((mod(bytes[1], 4)), 4), rsh(bytes[2], 4))] or '=', 
                    ((n- i) > 1) and _chars[lor(lsh(mod(bytes[2], 16), 2), rsh(bytes[3], 6))] or '=',
                    ((n- i) > 2) and _chars[mod(bytes[3], 64)] or '=')
    end
    return result
  end
  
  --- <summary> Decryption table. Uses URL Safe characters. </summary>
  local _bytes = {['A']= 0,['B']= 1,['C']= 2,['D']= 3,['E']= 4,['F']= 5,['G']= 6,['H']= 7,['I']= 8,['J']=9,
                  ['K']=10,['L']=11,['M']=12,['N']=13,['O']=14,['P']=15,['Q']=16,['R']=17,['S']=18,
                  ['T']=19,['U']=20,['V']=21,['W']=22,['X']=23,['Y']=24,['Z']=25,['a']=26,['b']=27,
                  ['c']=28,['d']=29,['e']=30,['f']=31,['g']=32,['h']=33,['i']=34,['j']=35,['k']=36,
                  ['l']=37,['m']=38,['n']=39,['o']=40,['p']=41,['q']=42,['r']=43,['s']=44,['t']=45,
                  ['u']=46,['v']=47,['w']=48,['x']=49,['y']=50,['z']=51,['0']=52,['1']=53,['2']=54,
                  ['3']=55,['4']=56,['5']=57,['6']=58,['7']=59,['8']=60,['9']=61,['-']=62,['_']=63,
                  ['=']=nil}
  
  --- <summary> Decodes input string from base64. The input string must not include null characters. </summary>
  --- <param name='value'> The string. </param>
  --- <example> <para>
  ---                                                   </para><para>
  --- _require = require 'require.lua'
  ---                                                   </para><para>
  --- _base64 = _require('base64.lua','<this chunk>')
  ---                                                   </para><para>
  --- print(_base64.fromBase64(_base64.toBase64('David'))) -- David
  ---                                                   </para><para>
  --- </para> </example>
  --- <namespace> _G </namespace>
  _base64.fromBase64 = function(value)

    local lsh = bitwise.lsh
    local rsh = bitwise.rsh
    local lor = bit.bitor
    local mod = math.mod
    
    local chars = {}
    local n = string.len(value)
    local result=''
    for i = 0, n - 1, 4 do
    
      for j = 1, 4  do 
        chars[j] = _bytes[(string.sub(value, (i + j), (i + j)) or '=')] 
      end
        
      result = string.format('%s%s%s%s', result, 
                  string.char(lor(lsh(chars[1], 2), rsh(chars[2], 4))),
                  (nil ~= chars[3]) and string.char(lor(lsh(chars[2], 4), rsh(chars[3], 2))) or '',
                  (nil ~= chars[4]) and string.char(lor(mod(lsh(chars[3], 6), 192), (chars[4]))) or '')
    end
    return result
  end

  -- Store chunk name at a standard location for require() to look it up.
  _G._LOADED[_chunkName] = _base64 

end
