--- <summary> Table functions to create (seal or lock) or unseal read only tables. </summary>
--- <license> (c) 2014 Integrated Scientific Resources, Inc.<para>
--- Licensed under The MIT License. </para><para>
--- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
--- BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
--- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. </para> </license>
--- <history date='04/30/14' by='David Hary' revision='4.0.5233.x'> Created. </history>
--- <tested date='09/08/14' by='David Hary' revision='4.0.5364.x' > 2601B, 2450 </history>

do

  local _chunkName = 'read.only.table.lua'

  --- <summary> table of read only table functions. </summary>
  local _table = {}

  --- <summary> Creates a copy of the table. </summary>
  --- <param name='lTable'> The table. </param>
  --- <example> <para>
  --- local _table = require 'read.only.table.lua'
  ---                                                   </para><para>
  --- local _os = _table.copy(_G.os)
  ---                                                   </para><para>
  --- print(_os.date())  -- Wed May 14 14:32:31 2014
  ---                       Wed Aug 06 08:14:27 2014 on 2601B 3.0.3
  ---                                                   </para><para>
  --- </para> </example>
  --- <returns> A copy of the table. </returns> 
  _table.copy = function(lTable) 
    local lNewTable = {}
    -- Copy original entries
    for lKey, lValue in pairs(lTable) do
        lNewTable[lKey] = lValue
    end
    return lNewTable
  end
  
  --- <summary> Throws a read only table exception. </summary>
  local _error = function(t, name, value)
    if nil == t[name] then
      error(name ..' cannot be added to a read only table', 2)
    else
      error(name ..' cannot be changed in a read only table', 2)
    end
  end
  
  --- <summary> Creates a read-only copy of the table.
  --- Maps the exiting table to a new table throwing an error if new
  --- function. </summary>
  --- <param name='lTable'> The existing table. </param>
  --- <param name='lockedTable'> The table to lock; new table if nil. </param>
  --- <example> <para>
  ---                                                   </para><para>
  --- continue from the above example:
  --- local _os2 = _table.lock(_os)
  ---                                                   </para><para>
  --- print(_os2.date())  -- Wed May 14 14:44:58 2014
  ---                        Wed Aug 06 08:14:27 2014 on 2601B 3.0.3
  ---                                                   </para><para>
  --- _os2.getenv = function() return 'env' end  
  ---             error:
  ---             -286, TSP Runtime error at line 1: getenv cannot be changed in a read only table 
  ---                                                   </para><para>
  --- _os2.setenv = function(value) return tostring(value) end  
  ---             error:
  ---             -286, TSP Runtime error at line 1: setenv cannot be added to a read only table 
  ---                                                   </para><para>
  --- local _t = {'k1','v1'}
  ---                                                   </para><para>
  --- print(_t[1])  -- k1
  ---                                                   </para><para>
  --- local _table = require 'read.only.table.lua'
  ---                                                   </para><para>
  --- local t = _table.lock(_t)
  ---                                                   </para><para>
  --- print(t[1])  -- k1
  ---                                                   </para><para>
  --- t[1] = 3   -- error:
  ---            -286, TSP Runtime error at line 1: 1 is a read only variable
  ---                                                   </para><para>
  --- t.x = 'x1' -- error:
  ---            -286, TSP Runtime error at line 1: x cannot be added to a read only table
  ---                                                   </para><para>
  --- </para> </example>
  --- <returns> A read-only table. </returns> 
  _table.lock = function(lTable, lockedTable)
      lockedTable = lockedTable or {}
      setmetatable(lockedTable, {__index = lTable, __newindex = _error})
      return lockedTable
  end

  --- <summary> Sets a table element if new. Checks if table value exists. 
  --- Reports 'a read only variable' if table value exists; 
  --- Otherwise, add the name-value pair to the table. </summary>
  local _rawset = function(t, name, value)
    if nil == t[name] then
      rawset(t, name, value) -- t[name] = value
    else
      error(name ..' is a read only variable', 2)
    end
  end

  --- <summary> Creates an add-only copy of the table where existing elements
  --- are read only.
  --- Maps the exiting table to a new table using the raw set if new
	--- function. </summary>
	--- <param name='lTable'> The existing table. </param>
  --- <param name='sealedTable'> The table to seal; new table if nil. </param>
  --- <example> <para>
  ---                                                   </para><para>
  --- continue from the above example:
  --- local _os2 = _table.seal(_os)
  ---                                                   </para><para>
  --- print(_os2.date())  -- Wed May 14 14:42:22 2014
  ---                                                   </para><para>
  --- _os2.getenv = function() return 'env' end  -- error 
  ---                                                   </para><para>
  --- local _t = {'k1','v1'}
  ---                                                   </para><para>
  --- print(_t[1]) 
  ---                                                   </para><para>
  --- local _table = require 'read.only.table.lua'
  ---                                                   </para><para>
  --- local t = _table.seal(_t)
  ---                                                   </para><para>
  --- print(t[1])
  ---                                                   </para><para>
  --- t[1] = 3   -- error
  ---                                                   </para><para>
  --- t.x = 'x1' -- okay 2601B 
  --- print (t.x) -- x1
  ---
  --- _s = _table.unseal(t)
  --- _s[1] = 3   -- error
  ---                                                   </para><para>
  --- </para> </example>
	--- <returns> A table where existing elements are read only. </returns> 
  _table.seal = function(lTable, sealedTable)
      if nil == sealedTable then sealedTable = {} end
      setmetatable(sealedTable, {__index = lTable, __newindex = _rawset})
      return sealedTable
	end
	
  --- <summary> Creates a new unsealed copy of the sealed table. </summary>
  --- <param name='lTable'> The sealed table. </param>
  --- <example> <para>
  ---                                                   </para><para>
  --- local _d = _G.display
  ---                                                   </para><para>
  --- print(_d.SCREEN_USER_SWIPE) -- display.SCREEN_USER_SWIPE
  ---                                                   </para><para>
  --- _d.x = 1 -- error  
  ---                                                   </para><para>
  --- local _table = require 'read.only.table.lua'
  ---                                                   </para><para>
  --- _d = _table.unseal(_G.display)
  ---                                                   </para><para>
  --- print(_d.SCREEN_USER_SWIPE) -- display.SCREEN_USER_SWIPE
  --- _d.x = 1 -- okay
  ---                                                   </para><para>
  --- print(_d.x) -- 1.00000e+00
  --- TSP.1:
  --- print(display.USER)  -- 3.00000e+00
  --- print(_d.USER)       -- 3.00000e+00
  ---                                                   </para><para>
  --- </para> </example>
  --- <returns> An unsealed table. </returns> 
	_table.unseal = function(lTable)

    local lMetaTable = getmetatable(lTable)
    local lNewTable = {}
    local lNewMetaTable = {}
    local lValue

    -- Copy original meta table entries
    for lKey, lValue in pairs(lMetaTable) do
        lNewMetaTable[lKey] = lValue
    end

    -- Override the original entries with the new ones
    lNewMetaTable.__metatable = lNewMetaTable
    lNewMetaTable.__index = function(lTable, lKey)
      lTable = lTable or {} -- deal with 'Parameter 'lTable' is not used.
      local lGetter = lNewMetaTable.Getters[lKey]

      if (lGetter) then
        return lGetter(lValue)
      else
        return lNewMetaTable.Objects[lKey]
      end
    end
    lNewMetaTable.__newindex = function(lTable, lKey, lValue)
      lTable = lTable or {} -- deal with 'Parameter 'lTable' is not used.
      local lSetter = lNewMetaTable.Setters[lKey]

      if (lSetter) then
        lSetter(lValue)
      else
        rawset(lNewTable, lKey, lValue)
      end
    end

    setmetatable(lNewTable, lNewMetaTable)

    return lNewTable
	end
  
  -- Store chunk for require() to look up.
  _G._LOADED[_chunkName] = _table
  
end
