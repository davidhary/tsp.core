--- <summary> Creates a read only version information table. </summary>
--- <license> (c) 2014 Integrated Scientific Resources, Inc.<para>
--- Licensed under The MIT License. </para><para>
--- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
--- BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
--- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. </para> </license>
--- <history date='04/30/14' by='David Hary' revision='4.0.5233.x'> Created. </history>
--- <tested date='09/11/14' by='David Hary' revision='4.0.5367.x' > 2601B, 2450 </history>

do

  local _chunkName = 'isr.version.info.lua'
  local _require = require 'require.lua'
  _require('read.only.table.lua', _chunkName)
  
  --- <summary> Gets or sets a namespace for ISR. </summary>
  isr = isr or {}

  --- <summary> Gets or sets a namespace for ISR version. </summary>
  if 'function' == type(isr.version) then
    -- allows loading this on top of legacy ISR Tsp code.
    isr.version = {}
  end
  isr.version = isr.version or {}

  local _info = {}

  --- <summary> Gets or sets a namespace for ISR version info. </summary>
  isr.version.info = _info

  --- <summary> Initializes the version information. </summary>
  --- <param name='value'> The version info table. </param>
  --- <returns> The modified version info table. </returns>
  _info.initialize = function(value)
    value.version = string.format('%d.%d.%d', value.major, value.minor, value.day)
    -- only major and minor are relevant 
    value.release = value.major + 0.1 * value.minor
    return value
  end
  
  --- <summary> The empty version information table. </summary>
  _info.empty = {major = nil, minor = nil, day = nil,   
                version = nil,  -- set using the version info table when run 
                release = nil,  -- only major and minor are relevant
                } 

  --- <summary> Create a sealed version info table. </summary>
  --- <param name='major'> The major revision. </param>
  --- <param name='minor'> The minor revision. </param>
  --- <param name='day'> The day revision (day since 1/1/2000). </param>
  --- <example> <para>
  --- _chunkName = '<this chunk>'
  ---                                                   </para><para>
  --- vi = isr.version.info.new(4, 0, 5367)
  ---                                                   </para><para>
  --- print(vi.version) -- 4.0.5367
  ---                                                   </para><para>
  --- _require = require 'require.lua'
  ---                                                   </para><para>
  --- _vi = _require('isr.version.info.lua', _chunkName)
  ---                                                   </para><para>
  --- _vnfo = _vi.new(4, 0, 5367)
  ---                                                   </para><para>
  --- print(_vnfo.version) -- 4.0.5367
  ---                                                   </para><para>
  --- </para> </example> 
  ---                                                   </para><para>
  --- <returns> A sealed version info table. </returns>
  _info.new = function(major, minor, day) 
    local _table = _require('read.only.table.lua')
    local _vi = _table.copy(_info.empty)
    _vi.major = major
    _vi.minor = minor
    _vi.day = day
    _info.initialize(_vi)
    return _table.seal(_vi)
  end

  -- Store chunk for require() to look up.
  _G._LOADED[_chunkName] = _info

end
