--- <summary> Includes array functions. </summary>
--- <remarks> The array functions were written so as to work with TSP buffers. </remarks>
--- <namespace> isr.array </namespace>
--- <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
--- Licensed under The MIT License. </para><para>
--- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
--- BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
--- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--- </para> </license>
--- <history date='03/24/05' by='David Hary' revision='1.0.1909.x'> Created </history>

do

  local _chunkName = 'isr.array.printing.lua'

  -- aliases
  local _require = require 'require.lua'
  _require('isr.array.lua', _chunkName)

  --- <summary> Defines the ISR name space. </summary>
  isr = isr or {}

  --- <summary> Defines the ISR array name space. </summary>
  isr.array = isr.array or {}

  local Public = isr.array
  -- Get local access to the global environment
  local _G = _G
  -- Declare all public functions as global variables and they will go to 
  -- a separate table automatically. Register this table as the package name. 
  setfenv(1, Public)

  --- <summary> Prints the array index and value for each element. </summary>
  --- <param name='a'> The array. </param>
  --- <param name='format'> The print format ('%d, %f'). </param>
  --- <param name='header'> The print header (nil). </param>
  --- <remarks> Does not work with TSP buffers.
  --- Tested 08/02/2008. </remarks>
  printArray = function(a, format, header)

    if nil ~= header then
      _G.print(header)
    end

    if nil == format then
      format = '%f' 
    end
    for i, v in _G.ipairs(a) do
      _G.print(_G.string.format (format, v))
    end

  end

  --- <summary> Prints the array index and value for each element. </summary>
  --- <param name='a'> The array. </param>
  --- <param name='format'> The print format ('%d, %f'). </param>
  --- <param name='header'> The print header (nil). </param>
  --- <remarks> Does not work with TSP buffers.
  --- Tested 08/02/2008. </remarks>
  printArrayIndexValue = function(a, format, header)

    if nil ~= header then
      _G.print(header)
    end

    if nil == format then
      format = '%d, %f' 
    end
    for i, v in _G.ipairs(a) do
      _G.print(_G.string.format (format, i, v))
    end

  end

  --- <summary> Prints the values of the two arrays of the same size. </summary>
  --- <param name='a'> The first array. </param>
  --- <param name='b'> The second array. </param>
  --- <param name='format'> The print format ('%f, %f'). </param>
  --- <param name='header'> The print header (nil). </param>
  --- <remarks> Does not work with TSP buffers.
  --- Tested 08/02/2008. </remarks>
  printTwoArrays = function(a, b, format, header)

    if nil ~= header then
      _G.print(header)
    end

    if nil == format then
      format = '%f, %f' 
    end

    for i, v in _G.ipairs(a) do
      _G.print(_G.string.format (format, v, b[i]))
    end

  end

  --- <summary> Prints the index and values of the two arrays of the same size. </summary>
  --- <param name='a'> The first array. </param>
  --- <param name='b'> The second array. </param>
  --- <param name='format'> The print format ('%f, %f'). </param>
  --- <param name='header'> The print header (nil). </param>
  --- <remarks> Does not work with TSP buffers.
  --- Tested 08/02/2008. </remarks>
  printTwoArraysIndexValues = function (a, b, format, header)

    if nil ~= header then
      _G.print(header)
    end

    if nil == format then
      format = '%d, %f, %f' 
    end

    for i, v in _G.ipairs(a) do
      _G.print(_G.string.format (format, i, v, b[i]))
    end

  end

  --- <summary> Print the values of the two arrays of the same size. </summary>
  --- <param name='a'> The first array. </param>
  --- <param name='b'> The second array. </param>
  --- <param name='c'> The third array. </param>
  --- <param name='format'> The print format ('%f, %f, %f'). </param>
  --- <param name='header'> The print header (nil). </param>
  --- <remarks> Does not work with TSP buffers.
  --- Tested 08/02/2008. </remarks>
  printThreeArrays = function(a, b, c, format, header)

    if nil ~= header then
      _G.print(header)
    end

    if nil == format then
      format = '%f, %f, %f' 
    end

    for i, v in _G.ipairs(a) do
      _G.print(_G.string.format (format, v, b[i], c[i]))
    end

  end

  --- <summary> Print the index values of the two arrays of the same size. </summary>
  --- <param name='a'> The first array. </param>
  --- <param name='b'> The second array. </param>
  --- <param name='c'> The third array. </param>
  --- <param name='format'> The print format ('%f, %f, %f'). </param>
  --- <param name='header'> The print header (nil). </param>
  --- <remarks> Does not work with TSP buffers.
  --- Tested 08/02/2008. </remarks>
  printThreeArraysIndexValues = function(a, b, c, format, header)

    if nil ~= header then
      _G.print(header)
    end

    if nil == format then
      format = '%d, %f, %f, %f' 
    end

    for i, v in _G.ipairs(a) do
      _G.print(_G.string.format (format, i, v, b[i], c[i]))
    end

  end

  -- return was commented out and the reset of the environment was added to fix
  -- the problem of packages not loading in a configuration sequence.
  _G.setfenv(1, _G)

  -- Store chunk name at a standard location for require() to look it up.
  _G._LOADED[_chunkName] = isr.array

end
