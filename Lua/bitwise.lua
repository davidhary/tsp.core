--- <summary> Extends the Lua bit functions. </summary>
--- <namespace> _G </namespace>
--- <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
--- Licensed under The MIT License. </para><para>
--- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
--- BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
--- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--- </para> </license>
--- <history date='03/21/09' by='David Hary' revision='2.1.2657.x'> 
--- lsh, rsh, bit, lor from working Lua base64 codec (c) 2006-2008 by Alex Kloss
--- http://www.it-rfc.de
--- licensed under the terms of the LGPL2 </history>
--- <history date='09/26/10' by='David Hary' revision='2.2.3921.x'> 
--- Adds bitwise function that treat nil as zero. </history>
--- <tested date='09/11/14' by='David Hary' revision='4.0.5367.x' > 2601B, 2450 </history>

do

  local _chunkName = 'g.bitwise.lua'

  --- <summary> Defines table for custom bit-wise operations. </summary>
  bitwise = bitwise or {}

  --- <summary> Implements bitwise AND treating nil values as zero. </summary>
  --- <param name='left'> The left hand value. </param>
  --- <param name='right'> The right hand value. </param>
  --- <example> <para>
  ---                               </para><para> 
  --- print(bitwise.bitand(nil, 7)) -- 0.00000e+00
  ---                               </para><para> 
  --- print(bit.bitand(nil, 7))   -- -104 data error.</para><para>
  ---                               </para><para> 
  --- print(bitwise.bitand(7, 6)) -- 6.00000e+00 
  ---                               </para><para> 
  --- print(bit.bitand(0, 7))  -- 0.00000e+00      
  ---                               </para><para> 
  --- </para> </example>
  --- <namespace> _G </namespace>
  --- <returns> left AND right. </returns>  
  bitwise.bitand = function(left, right)
    return bit.bitand(left or 0, right or 0)
  end

  --- <summary> Checks if all bits of the right value as set in the left value. </summary>
  --- <param name='left'> The left hand value. </param>
  --- <param name='right'> The right hand value. </param>
  --- <example> <para>
  ---                               </para><para> 
  --- print(bitwise.allset(1, 1)) -- true
  ---                               </para><para> 
  --- print(bitwise.allset(3, 3)) -- true
  ---                               </para><para> 
  --- print(bitwise.allset(1, 3)) -- false
  ---                               </para><para> 
  --- </para> </example>
  --- <namespace> _G </namespace>
  --- <returns> <c>true</c> if all the right bits are set in the left value. </returns>  
  bitwise.allset = function(left, right)
    return bitwise.bitand(left, right) == right 
  end
  
  --- <summary> Checks if any bit of the right value as set in the left value. </summary>
  --- <param name='left'> The left hand value. </param>
  --- <param name='right'> The right hand value. </param>
  --- <example> <para>
  ---                               </para><para> 
  --- print(bitwise.anyset(1, 1)) -- true
  ---                               </para><para> 
  --- print(bitwise.anyset(1, 3)) -- true
  ---                               </para><para> 
  --- print(bitwise.anyset(1, 6)) -- false
  ---                               </para><para> 
  --- </para> </example>
  --- <namespace> _G </namespace>
  --- <returns> <c>true</c> if all the right bits are set in the left value. </returns>  
  bitwise.anyset = function(left, right)
    return bitwise.bitand(left, right) ~= 0  
  end
  
  --- <summary> Clears the bits that on in the right hand value. </summary>
  --- <param name='left'> The left hand value. </param>
  --- <param name='right'> The right hand value. </param>
  --- <example> <para>
  ---                               </para><para> 
  --- print(bitwise.clear(7, nil)) -- 7.00000e+00
  ---                               </para><para> 
  --- print(bitwise.clear(7, 6))   -- 1.00000e+00
  ---                               </para><para> 
  --- </para> </example>
  --- <namespace> _G </namespace>
  --- <returns> left - (left AND right). </returns>  
  bitwise.clear = function(left, right)
    left = left or 0
    return left - bitwise.bitand(left, right)
  end
  
  --- <summary> Implements bitwise OR treating nil values as zero. </summary>
  --- <param name='left'> The left hand value. </param>
  --- <param name='right'> The right hand value. </param>
  --- <example> <para>
  ---                               </para><para> 
  --- print(bitwise.bitor(nil, 7)) -- 7.00000e+00
  ---                               </para><para> 
  --- print(bitwise.bitor(6, 7)) -- 7.00000e+00
  ---                               </para><para> 
  --- </para> </example>
  --- <namespace> _G </namespace>
  --- <returns> left OR right. </returns>  
  bitwise.bitor = function(left, right)
    return bit.bitor(left or 0, right or 0)
  end

  --- <summary> Implements bitwise XOR treating nil values as zero. </summary>
  --- <param name='left'> The left hand value. </param>
  --- <param name='right'> The right hand value. </param>
  --- <example> <para>
  ---                               </para><para> 
  --- print(bitwise.bitxor(nil, 7)) -- 7.00000e+00
  ---                               </para><para> 
  --- print(bitwise.bitxor(6, 7)) -- 1.00000e+00
  ---                               </para><para> 
  --- </para> </example>
  --- <namespace> _G </namespace>
  --- <returns> left XOR right. </returns>  
  bitwise.bitxor = function(left, right)
    return bit.bitxor(left or 0, right or 0)
  end

  --- <summary> Calculates a Bitwise NOT. </summary>
  --- <param name='value'> The value which bits are to bit toggled. </param>
  --- <param name='wordSize'> The word size in bits to return.
  --- If nil, the word sizes extends over the word size of the value.
  --- For example, 3 has two bits and 4 has 3. </param>
  --- <example> <para>
  ---                               </para><para> 
  --- print(bitwise.bitnot(nil, 1)) -- 1.00000e+00
  ---                               </para><para> 
  --- print(bitwise.bitnot(6)) -- 1.00000e+00
  ---                               </para><para> 
  --- print(bitwise.bitnot(9)) -- 6.00000e+00
  ---                               </para><para> 
  --- </para> </example>
  --- <namespace> _G </namespace>
  --- <returns> toggle(value). </returns>  
  bitwise.bitnot = function(value, wordSize)
    value = value or 0
    if nil == wordSize then
      _, wordSize = math.frexp(value)
    end 
    local a = value
    for i = 1, wordSize do
      a = bit.toggle(a, i)
    end
    return a
  end
  
  --- <summary> Returns left shifted bits. </summary>
  --- <param name='value'> The value. </param>
  --- <param name='shift'> The number of positions to shift. </param>
  --- <returns> left-shifted value. </returns>  
  bitwise.lsh = function(value, shift)
    value = value or 0
    return math.mod(value*(2^shift), 256)
  end
  
  --- <summary> Returns right shifted bits. </summary>
  --- <param name='value'> The value. </param>
  --- <param name='shift'> The number of positions to shift. </param>
  --- <returns> right-shifted value. </returns>  
  bitwise.rsh = function(value, shift)
    value = value or 0
    return math.mod(math.floor(value/2^shift), 256) 
  end
  
  --- <summary> Checks if the specified bit is set (for OR) </summary>
  --- <param name='value'> The value. </param>
  --- <param name='b'> The bit position (zero based). </param>
  --- <returns> <c>true</c> if the specified bit is on. </returns>  
  bitwise.biton = function(value, b)
    value = value or 0
    local bitValue = 2^(b-1)
    return math.mod(value, 2 * bitValue) > math.mod(value, bitValue)
  end
  
  --- <summary> Returns logic OR for number values.
  --- same as <see cref='bit.bitor'/>. </summary>
  --- <param name='left'> The left hand value. </param>
  --- <param name='right'> The right hand value. </param>
  --- <param name='wordSize'> Specifies word size. Defaults to 8 bits. </param>
  --- <returns> left or right within word size. </returns>  
  bitwise.lor = function(left, right, wordSize)
    if nil == wordSize then
      wordSize = 8
    end
    local bitValue = 1
    local result = 0
    local biton = bitwise.biton
    for p = 1, wordSize do 
      result = result + ((biton(left, p) or biton(right, p)) and bitValue or 0) 
      bitValue = 2 * bitValue
    end
    return result
  end
  
  -- Store chunk name at a standard location for require() to look it up.
  _G._LOADED[_chunkName] = bitwise

end
