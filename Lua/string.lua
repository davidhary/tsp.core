--- <summary> String functions. </summary>
--- <namespace> _G </namespace>
--- <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
--- Licensed under The MIT License. </para><para>
--- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
--- BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
--- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--- </para> </license>
--- <history date='08/12/09' by='David Hary' revision='2.2.3511.x'> Created </history>

do

  local _chunkName = 'g.string.lua'

  --- <summary> Prints the string line by line. </summary>
  --- <param name='value'> The string. </param>
  --- <example> <para>
  --- string.printLines(string.char(13, 10) .. '<hello world >' .. string.char(13, 10)  .. '< from >' .. string.char(13, 10)  .. '< Lua>' .. string.char(13, 10))
  --- string.printLines(isr_core.source) 
  --- </para> </example>
  string.printLines = function(value) 

    local i = 1
    local k,l = 1,1
    while nil ~= k do
      k,l = string.find(value, '%c+', i)
      if nil ~= k then
        if k > i then 
          print(string.sub(value, i, k - 1))
        end
        if nil ~= l then
          i = l + 1
        elseif nil ~= k then
          i = k + 1
        end
      end
    end
    
  end
  
  --- <summary> Checks if the token is located. </summary>
  --- <param name='value'> The string. </param>
  --- <param name='value'> The token to look for. </param>
  string.contains = function(value, token)
    return nil ~= string.find(value, token, 1, true)
  end
  
  --- <summary> Trims spaces from the begging and end of string. </summary>
  --- <param name='value'> The string. </param>
  string.trim = function(value)
    return string.gsub(value, '^%s*(.-)%s*$', '%1')
  end
  
  --- <summary> Replaces a section of a string with new value. </summary>
  --- <param name='value'> The string. </param>
  --- <param name='newValue'> The new string. </param>
  --- <param name='position'> The starting position of the new value. </param>
  string.replaceAt = function(value, newValue, position)
    local len = string.len(value)
    local s = string.sub(string.sub(value, 1, position-1) .. newValue, 1, len)
    local n = len - string.len(s)
    if n > 0 then
      s = s .. string.sub(value, -n)
    end           
    return s
  end

  --- <summary> Replaces all occurrences with new values. </summary>
  --- <param name='value'> The string. </param>
  --- <param name='oldValue'> The old value. </param>
  --- <param name='newValue'> The new value. </param>
  --- <example> <para>
  ---                                               </para><para>
  --- print(string.replace(string.format('%7.4f', 0 ), " ", "0"))
  ---                                               </para><para>
  --- print(string.replace("David","a","z"))
  --- </para> </example> 
  string.replace = function(value, oldValue, newValue)
    local first = 1
    local pos = 1
    while first do
      first, pos = _G.string.find(value, oldValue, pos, true)
      if first then -- found?
        value = string.replaceAt(value, newValue, first)
        pos = first + string.len(newValue)
      end
    end
    return value
  end

  --- <summary> Joins a table of strings using the delimiter as separator.
  --- The order of the join is not defined for non-numeric indexed
  --- tables. Numeric indexed tables seem okay. </summary>
  --- <param name='value'> The string. </param>
  --- <param name='delimiter'> The delimiter. </param>
  --- <example> <para>
  ---                                               </para><para>
  --- string.join({'Anna','Bob','Charlie','Dolores'}, ', ') 
  ---                                               </para><para>
  --- string.join({Anna='Anna',bob='Bob',chuck='Charlie',Dolly='Dolores'}, ', ') 
  ---                                               </para><para>
  --- </para> </example>
  string.join = function(table, delimiter)
  
    local value = nil
    if nil ~= table then
	    for k, v in pairs(table) do
	      if nil ~= value then
	        value = value .. delimiter
	      else
	        value = '' 
	      end
	      value = value .. v
	    end
    end
    value = value or ''
    return value    
    
  end
  
  --- <summary> Joins the name of table elements using the delimiter as separator.
  --- The order of the join is not defined for non-numeric indexed
  --- tables. </summary>
  --- <param name='value'> The string. </param>
  --- <param name='delimiter'> The delimiter. </param>
  --- <example> <para>
  --- string.joinNames({Anna = 1, Bob = 2, Charlie = 2, Dolores=4}, ', ') 
  ---                                               </para><para>
  --- string.joinNames({Anna='Anna',bob='Bob',chuck='Charlie',Dolly='Dolores'}, ', ') 
  ---                                               </para><para>
  --- </para> </example>
  string.joinNames = function(table, delimiter)
  
    local value = nil
    if nil ~= table then
      for k, v in pairs(table) do
        if nil ~= value then
          value = value .. delimiter 
        else
          value = '' 
        end
        value = value .. k
      end
    end
    value = value or ''
    return value    
    
  end

  --- <summary> Returns the string byte values. </summary>
  --- <param name='value'> The string. </param>
  --- <returns> An indexed table of bytes. </returns>
  string.toBytes = function(value)
    local values = {}
    if value and string.len(value) > 0 then
      for i = 1, string.len(value) do
        values[i] = string.byte(value, i)
      end
    end 
    return values
  end
    
  --- <summary> Splits text into a list consisting of the strings in text,
  --- separated by strings matching pattern. </summary>
  --- <param name='value'> The string. </param>
  --- <param name='pattern'> The search pattern. </param>
  --- <param name='plain'> <c>true</c> to use non patterned search. </param>
  --- <returns> A table of split strings. </returns>
  --- <example> <para>
  --- string.split('Anna, Bob, Charlie,Dolores', ',%s*')
  --- string.join(string.split('Anna, Bob, Charlie, Dolores', ',%s*'), '* ')
  --- </para> </example>
  string.split = function(value, pattern, plain)
    local list = {}
    local pos = 1
    if _G.string.find('', pattern, 1) then -- this would result in endless loops
      _G.error('pattern matches empty string!')
    end
    while 1 do
      local first, last = _G.string.find(value, pattern, pos, plain)
      if first then -- found?
        _G.table.insert(list, _G.string.sub(value, pos, first-1))
        pos = last + 1
      else
        _G.table.insert(list, _G.string.sub(value, pos))
        break
      end
    end
    return list
  end

  --- <summary> Checks if the string starts with the specified value. </summary>
  --- <param name='s'> The string. </param>
  --- <param name='value'> The search value. </param>
  --- <returns> <c>true</c> if the string starts with the specified value. </returns>
  --- <example> <para>
  --- print(string.startsWith('David', 'd'))
  --- print(string.startsWith('David', 'D'))
  --- print(string.startsWith('David', 'Da'))
  --- print(string.startsWith('David', 'Dav'))
  --- print(string.startsWith('David', 'Davi'))
  --- </para> </example>
  string.startsWith = function(s, value)
    return value == _G.string.sub(s, 1, _G.string.len(value or '')) 
  end

  --- <summary> Parses the version to a number. </summary>
  --- <returns> The version (major.minor) or 0 if parse failed. </returns>
  --- <example> <para>
  --- print(string.tryParseVersion(_G.localnode.version))
  --- </example> </para>
  string.tryParseVersion = function(version)
    local rev = 0
    local vi = string.split(version, '.', true)
    if _G.table.getn(vi) >= 2 then
      local major = _G.tonumber(vi[1]) 
      local minor = _G.tonumber(vi[2])
      if minor and major then
        rev = major + 0.1 * minor 
      end 
    end
    return rev
  end
  
  -- Store chunk name at a standard location for require() to look it up.
  _G._LOADED[_chunkName] = string

end
