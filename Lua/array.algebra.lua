--- <summary> Array algebra functions. </summary>
--- <remarks> The array functions were written so as to work with TSP buffers. </remarks>
--- <namespace> _G.isr.array </namespace>
--- <license> (c) 2005 Integrated Scientific Resources, Inc. <para>
--- Licensed under The MIT License. </para><para>
--- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
--- BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
--- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--- </para> </license>
--- <history date='03/24/05' by='David Hary' revision='1.0.1909.x'> Created </history>

do

  local _chunkName = 'isr.array.algebra.lua'

  -- aliases
  local _require = require 'require.lua'
  _require('isr.array.lua', _chunkName)

  --- <summary> Defines the ISR name space. </summary>
  isr = isr or {}

  --- <summary> Defines the ISR array name space. </summary>
  isr.array = isr.array or {}

  local Public = isr.array
  -- Get local access to the global environment
  local _G = _G
  -- Declare all public functions as global variables and they will go to 
  -- a separate table automatically. Register this table as the package name. 
  setfenv(1, Public)

  --- <summary> Returns a new array that is the addition of two arrays. </summary>
  --- <param name='a'> The first array. </param>
  --- <param name='b'> The second array. </param>
  --- <param name='from'> The starting index (1). </param>
  --- <param name='to'> The ending index (length). </param>
  --- <remarks> the 'from' and 'to' arguments must be provided for
  ---   TSP buffers. </remarks>
  --- <example> <para>
  ---                                   </para><para> 
  --- a = isr.array.add({1,2,3},-1,2,3) -- 1,2
  ---                                   </para><para> 
  --- isr.array.listValues(a, '%d')
  ---                                   </para><para> 
  --- </para> </example>
  --- <returns> Array. </returns> 
  add = function(a, b, from, to)

    local i, j = from or 1, to or getn(a)

    local newArray = {}
    setn(newArray, j - i + 1)
    for k = i, j  do
      newArray[k - i + 1] = a[k] + b[k]
    end

    return newArray

  end

  --- <summary> OR's all array elements. </summary>
  --- <param name='a'> Array or buffer. </param>
  --- <param name='from'> The starting index (1). </param>
  --- <param name='to'> The ending index (length). </param>
  --- <example> <para>
  ---                                   </para><para> 
  --- print(isr.array.bitOr({1,2,4},2,3) -- 6
  ---                                   </para><para> 
  --- </para> </example>
  --- <returns> The overall status. </returns> 
  bitOr = function(a, from, to) 
    
    local status = 0
    local bitOr = _G.bit.bitor
    local i, j = from or 1, to or getn(a)
    for k = i, j  do
      status = bitOr(status, a[k])
    end
    return status 

  end

  --- <summary> Finds the minimum and maximum values. </summary>
  --- <param name='a'> The array. </param>
  --- <param name='from'> The starting index (1). </param>
  --- <param name='to'> The ending index (length). </param>
  --- <returns> Minimum, Maximum. </returns>
  --- <remarks> The 'from' and 'to' arguments must be provided for
  ---   TSP buffers. </remarks>
  --- <example> <para>
  ---                                   </para><para> 
  --- print(isr.array.extrema({1,2,4,5},2,3)) -- 2,4
  ---                                   </para><para> 
  --- </para> </example>
  --- <returns> minimum, maximum. </returns> 
  extrema = function(a, from, to)

    local i, j = from or 1, to or getn(a)
    if nil == a or 0 == from then return nil, nil end
    local min = a[i]
    local max = min
    for k = i, j  do
      local candidate = a[k]
      if candidate < min then 
        min = candidate
      elseif candidate > max then
        max = candidate
      end
    end

    return min, max

  end

  --- <summary> Returns the range of array values. </summary>
  --- <param name='a'> The array. </param>
  --- <param name='from'> The starting index (1). </param>
  --- <param name='to'> The ending index (length). </param>
  --- <returns> Maximum - minimum. </returns>
  --- <remarks> The 'from' and 'to' arguments must be provided for
  ---   TSP buffers. </remarks>
  --- <example> <para>
  ---                                   </para><para> 
  --- print(isr.array.range({1,2,4,5},2,3)) -- 2
  ---                                   </para><para> 
  --- </para> </example>
  --- <returns> Range. </returns> 
  range = function(a, from, to)

    local min, max = extrema(a, from, to)
    return max - min

  end

  --- <summary> Returns the product of two arrays. </summary>
  --- <param name='a'> The first array. </param>
  --- <param name='b'> The second array. </param>
  --- <param name='from'> The starting index (1). </param>
  --- <param name='to'> The ending index (length). </param>
  --- <remarks> The 'from' and 'to' arguments must be provided for
  ---   TSP buffers. </remarks>
  --- <example> <para>
  ---                                   </para><para> 
  --- a = isr.array.product({1,2,3},{1,2,3},2,3)
  ---                                   </para><para> 
  --- isr.array.listValues(a, '%d')
  ---                                   </para><para> 
  --- </para> </example>
  --- <returns> Array. </returns> 
  product = function(a, b, from, to)

    local i, j = from or 1, to or getn(a)

    local newArray = {}
    setn(newArray, j - i + 1)
    for item = i, j  do
      newArray[item - i + 1] = a[item] * b[item]
    end

    return newArray

  end

  --- <summary> Returns the array sum. </summary>
  --- <param name='a'> The array. </param>
  --- <param name='from'> The starting index (1). </param>
  --- <param name='to'> The ending index (length). </param>
  --- <remarks> The 'from' and 'to' arguments must be provided for
  ---   TSP buffers. </remarks>
  --- <example> <para>
  ---                                   </para><para> 
  --- print(isr.array.sum({1,2,4,5},2,3)) -- 6
  ---                                   </para><para> 
  --- </para> </example>
  --- <returns> Sum. </returns> 
  sum = function(a, from, to)

    local i, j = from or 1, to or getn(a)

    local sum = 0
    for k = i, j  do
      sum = sum + a[k]
    end

    return sum

  end

  -- return was commented out and the reset of the environment was added to fix
  -- the problem of packages not loading in a configuration sequence.
  _G.setfenv(1, _G)

  -- Store chunk name at a standard location for require() to look it up.
  _G._LOADED[_chunkName] = isr.array

end
