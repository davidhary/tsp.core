--- <summary> Enables the management of loaded scripts. </summary>
--- <namespace> _G </namespace>
--- <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
--- Licensed under The MIT License. </para><para>
--- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
--- BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
--- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--- </para> </license>
--- <history date='03/15/07' by='David Hary' revision='1.0.2630.x'> Created </history>
--- <tested date='09/11/14' by='David Hary' revision='4.0.5367.x' > 2601B, 2450 </history>

do

  local _chunkName = 'require.lua'

  --- <summary> Gets or sets the sentinel indicating if the instrument supports TSP 2.0. </summary>
  --- <remarks> Used here as acquire must be included in every script. <remarks>
  --- <value> <c>true</c> if the instrument supports tsp2. </value>
  supportsTsp2 = nil ~= _G.defbuffer1

  --- <summary> Gets a reference to the loaded chunk from the _LOADED table;  
  --- Throws an exception if the chunk was not loaded. </summary>
  --- <param name='requiredName'> The name of the required script or chuck. </param>
  --- <param name='currentSName'> The name of the current script or chuck.
  --- Defaults to 'this chunk'. </param>
  --- <param name='throw'> The condition for throwing an error.  Defaults to true. </param>
  --- <remarks> It is not clprint(require)ear how TSP uses TSP. Loaded scripts are not listed.
  --- This implementation overloads the exiting implementation. 
  --- </remarks>
  --- <example> <para>
  ---                                                   </para><para>
  --- local _chunkName = '_info.lua'
  ---                                                   </para><para>
  --- local _info = function() return {version ='1.0.0', title='test'} end
  ---                                                   </para><para>
  --- _G._LOADED[_chunkName] = _info
  ---                                                   </para><para>
  --- _require = require 'require.lua'
  ---                                                   </para><para>
  --- local _info1 = _require('_info.lua', '<new chunk name>')
  ---                                                   </para><para>
  --- print(_info1().version) -- 1.0.0
  --- print(_info().version)  -- 1.0.0 
  ---                                                   </para><para>
  --- for n in _G._LOADED do print(n) end
  ---                                                   </para><para>
  --- </para> </example> 
  --- <namespace> _G </namespace>
  --- <returns> References to the loaded chunk. </returns>
  local _require = function(requiredName, currentName, throw)
    throw = throw or true
    currentName = currentName or 'this chunk'
    if _LOADED[requiredName] ~= nil then 
      return _LOADED[requiredName]
    elseif throw then
      error(requiredName .. ' must be loaded before loading ' .. currentName, 0)
    else
      return nil
    end
  end
  
  --- <summary> This replaces the require function in TSP.1. </summary>
  if not supportsTsp2 then 
    require = _require
  end
  
  -- Store chunk for require() to look it up.
  _G._LOADED[_chunkName] = _require

end
