## ISR Tsp Lua<sub>&trade;</sub>: Lua code for the ISR Test Script Processor Framework
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*4.0.5242 05/09/14*<br/>	Renames string replace to replace at and adds string
replace. Split array code into separate files for algebra, filtering,
and interpolation.

*4.0.5237 05/04/14*<br/>	Indexes the string functions. Adds join and split.

*4.0.5233 04/30/14*<br/>	Removes Commit history. Uses MIT license. Adds spaces
to comments. Removes new lines in comments.

*2.2.4232 08/03/11*<br/>	Standardize code elements and documentation.

*2.2.3876 08/12/10*<br/>	Adds string functions to handle a stack trace. Adds a
custom XPCALL function/

*2.2.3567 10/07/09*<br/>	Released with general support.

*2.2.3562 10/02/09*<br/>	Released with general support.

*2.2.3539 09/09/09*<br/>	Adds table functions such as length and lower and
upper bounds.

*2.2.3511 08/12/09*<br/>	Adds string global script with print line code.

*2.1.3366 03/21/09*<br/>	Adds left and right shift and bit on commands to bit.

*1.0.2643 03/28/07*<br/>	Add print f and bit not functions.

\(C\) 2007 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Eclipse ® from the
[Eclipse Foundation](http://www.eclipse.org).

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:[\
Lua Global Support Libraries](https://bitbucket.org/davidhary/tsp.core)[\
LUA](http://www.lua.org)[\
table.copy](http://www.loop.org)[\
Working LUA base-64 codec](http://www.it-rfc.de)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:[\
Test Script Builder](http://www.keithley.com)
