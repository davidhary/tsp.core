--[[
    luaunit.lua

Description: A unit testing framework
Homepage: http://phil.freehackers.org/luaunit/
Initial author: Ryu, Gwang (http://www.gpgstudy.com/gpgiki/LuaUnit)
improvements by Philippe Fremy <phil@freehackers.org>
Version: 1.2 

Changes between 1.2 and 1.1:
- tests are now run in alphabetical order
- fix a bug that would prevent all tests from being run

Changes between 1.1 and 1.0:
- internal variables are not global any more
- you can choose between assertEquals(actual, expected) or assertEquals(
  expected, actual)
- you can assert for an error: assertError(f, a, b) will assert that calling
  the function f(a,b) generates an error
- display the calling stack when an error is spotted
- a dedicated class collects and displays the result, to provide easy
  customisation
- two verbosity level, like in python unit test
--- <history date='03/05/07' by='David Hary' revision='1.02.2620.x'>
--- Add LuaUnit:runTestMethodSingle(aName, aMethod).  
--- Test method name points to a stand alone method and if so run that test.
--- Clear result each run.
--- Add a name space.
--- Add third verbosity level for debug mode.
--- </history>

]]--

do

	local _chunkName = 'g.LUnit.luaunit.lua'
	
  _G.LUnit = {}
  local Public = _G.LUnit
  -- Get local access to the global environment
  local _G = _G
  -- Declare all public functions as global variables and they will go to 
  -- a separate table automatically. Register this table as the package name. 
  setfenv(1, Public)
  
  argv = arg
  
  --- <summary> Asserts that calling f with the arguments will raise an error. </summary>
  --- <param name='f'> References a function. </param>
  --- <param name='...'> Function arguments. </param>
  --- <example> <para>
  --- assertError(f, 1, 2) => f(1,2) should generate an error.
  --- Function arguments.
  --- </para> </example>
  function assertError(f, ...)
    -- local has_error, error_msg = not _G.pcall(f, _G.unpack(arg))
    local has_error = not _G.pcall(f, _G.unpack(arg))
    if has_error then return end 
    _G.error('No error generated', 2)
  end

  --- <summary> Asserts that two whole values are equal. </summary>
  --- <param name='expected'> The expected value. </param>
  --- <param name='actual'> The actual value. </param>
  --- <param name='message'> The error message. </param>
  function assertEqualLongs(actual, expected,  message)

    if nil == actual then
      if nil ~= expected then
	      local errorMsg
	      errorMsg = _G.string.format('expected: %d, actual: nil', expected)
	      if nil ~= message then
	        errorMsg = message..'\n'..errorMsg
	      end
	      _G.print(errorMsg)
	      _G.error(errorMsg, 2)
			end
    elseif nil == expected then
      local errorMsg
      errorMsg = _G.string.format('expected: nil, actual: %d', actual)
      if nil ~= message then
        errorMsg = message..'\n'..errorMsg
      end
      _G.print(errorMsg)
      _G.error(errorMsg, 2)
    elseif  actual ~= expected  then
      local errorMsg
      errorMsg = _G.string.format('expected: %d, actual: %d', expected, actual)
      if nil ~= message then
        errorMsg = message..'\n'..errorMsg
      end
      _G.print(errorMsg)
      _G.error(errorMsg, 2)
    end
  end
  
  --- <summary> Asserts that two numeric values are equal. </summary>
  --- <param name='expected'> The expected value. </param>
  --- <param name='actual'> The actual value. </param>
  --- <param name='tolerance'> The maximum difference allowed. </param>
  --- <param name='message'> The error message. </param>
  function assertEqualValues(actual, expected, tolerance, message)
  
    if _G.math.abs(actual - expected) > tolerance then
      local errorMsg
      errorMsg = _G.string.format('expected: %f, actual: %f', expected, actual)
      if nil ~= message then
        errorMsg = message..'\n'..errorMsg
      end
      _G.print(errorMsg)
      _G.error(errorMsg, 2)
    end
  end
  
  --- <summary>Asserts that two strings are equal.</summary>
  --- <param name='expected'>The expected value.</param>
  --- <param name='actual'>The actual value.</param>
  --- <param name='message'>The error message.</param>
  function assertEqualStrings(actual, expected,  message)
  
    if  actual ~= expected  then
      local errorMsg
      errorMsg = _G.string.format('\nexpected: %s, \nactual: %s', expected, actual)
      if nil ~= message then
        errorMsg = message..'\n'..errorMsg
      end
      _G.print(errorMsg)
      _G.error(errorMsg, 2)
    end
  end

  --- <summary>Asserts that two values are equal within the tolerance.</summary>
  --- <param name='expected'>The expected value.</param>
  --- <param name='actual'>The actual value.</param>
  --- <param name='tolerance'>The maximum difference between the two values..</param>
  --- <param name='message'>The error message.</param>
  function assertClose(actual, expected, tolerance, message)
    
    if _G.math.abs(actual - expected) > tolerance  then
      local errorMsg
      errorMsg = _G.string.format('expected: %f, actual: %f, diff: ', expected, actual, tolerance)
      if nil ~= message then
        errorMsg = message..'\n'..errorMsg
      end
      _G.print (errorMsg)
      _G.error(errorMsg, 2)
    end
  end

  --- <summary>Asserts that the value is false.</summary>
  --- <param name='actual'>The actual value.</param>
  --- <param name='message'>The error message.</param>
  function assertEqualBooleans(actual, expected, message)
  
    if  actual ~= expected then
      local errorMsg
      errorMsg = _G.string.format('expected: %s, actual: %s', _G.tostring(expected), _G.tostring(actual))
      if nil ~= message then
        errorMsg = message..'\n'..errorMsg
      end
      _G.print(errorMsg)
      _G.error(errorMsg, 2)
    end

  end

  --- <summary>Asserts that the value is false.</summary>
  --- <param name='actual'>The actual value.</param>
  --- <param name='message'>The error message.</param>
  function assertNotNil(actual, message)
  
    if nil == actual then
      local errorMsg
      errorMsg = _G.string.format('expected not Nil')
      if nil ~= message then
        errorMsg = message..'\n'..errorMsg
      end
      _G.print(errorMsg)
      _G.error(errorMsg, 2)
    end

  end
  
  --- <summary>Asserts that the value is false.</summary>
  --- <param name='actual'>The actual value.</param>
  --- <param name='message'>The error message.</param>
  function assertFalse(actual, message)
  
  	return assertEqualBooleans(actual, false, message)

  end

  --- <summary>Asserts that the value is true.</summary>
  --- <param name='actual'>The actual value.</param>
  --- <param name='message'>The error message.</param>
  function assertTrue(actual, message)
  
  	return assertEqualBooleans(actual, true, message)

  end

  -- assert_equals = assertEquals
  -- assert_error = assertError
  
  function wrapFunctions(...)
    -- Use me to wrap a set of functions into a Runnable test class:
    -- TestToto = wrapFunctions(f1, f2, f3, f3, f5)
    -- Now, TestToto will be picked up by LuaUnit:run()
    local testClass, testFunction
    testClass = {}

    for i, testName in _G.ipairs(arg) do 
      testFunction = _G[testName]
      testClass[testName] = testFunction
		end
    return testClass
  end
  
  function __genOrderedIndex(t)
      local orderedIndex = {}
      for key in t do
         _G.table.insert(orderedIndex, key)
      end
      _G.table.sort(orderedIndex)
      return orderedIndex
  end
  
  function orderedNext(t, state)
      -- Equivalent of the next function, but returns the keys in the alphabetic
      -- order. We use a temporary ordered key table that is stored in the
      -- table being iterated.
  
    -- result:displayStatus('orderedNext: state = '..tostring(state)
      if nil == state then
          -- the first time, generate the index
          t.__orderedIndex = __genOrderedIndex(t)
          key = t.__orderedIndex[1]
          return key, t[key]
      end
      -- fetch the next value
      key = nil
      for i = 1,_G.table.getn(t.__orderedIndex) do
          if t.__orderedIndex[i] == state then
              key = t.__orderedIndex[i+1]
          end
      end
  
      if key then
          return key, t[key]
      end
  
      -- no more value to return, clean-up
      t.__orderedIndex = nil
      return
  end
  
  function orderedPairs(t)
      -- Equivalent of the pairs() function on tables. Allows to iterate
      -- in order
      return orderedNext, t, nil
  end

  -- class UnitResult Begin

  UnitResult = {-- class
    failureCount = 0,
    testCount = 0,
    errorList = {},
    currentClassName = '',
    currentTestName = '',
    testHasFailure = false,
    verbosity = 1
  }
  
  function UnitResult:clear()
    self.failureCount = 0
    self.testCount = 0
    self.errorList = {}
    self.currentClassName = ''
    self.currentTestName = ''
    self.testHasFailure = false
    self.verbosity = 1
  end

  function UnitResult:displayClassName()
    _G.print('\t'.. self.currentClassName)
  end

  function UnitResult:displayTestName()
    if self.verbosity > 0 then
      _G.print('\t\t'.. self.currentTestName)
    end
  end

  function UnitResult:displayStatus(message)
    if self.verbosity >= 3 then
      _G.print(message)
    end
  end

  function UnitResult:displayFailure(errorMsg)
    if 0 == self.verbosity then
      io.stdout:write('F')
    else
      _G.print(errorMsg)
      _G.print('Failed')
    end
  end

  function UnitResult:displaySuccess()
    if self.verbosity > 0 then
      --_G.print ('Okay')
    else 
      io.stdout:write('.')
    end
  end

  function UnitResult:displayOneFailedTest(failure)
    testName, errorMsg = _G.unpack(failure)
    _G.print('\t\t'..testName..' failed')
    _G.print(errorMsg)
  end

  function UnitResult:displayFailedTests()
  	if nil == self.errorList then return end
    if 0 == _G.table.getn(self.errorList) then return end
    local s = 'Failed tests:'
    _G.print(s)
    -- _G.print(_G.string.rep ('-', _G.string.len(s)))
    -- _G.table.foreachi(self.errorList, self.displayOneFailedTest)
    for i, v in _G.ipairs(self.errorList) do 
    	self.displayOneFailedTest(i, v)
    end
    _G.print()
  end

  function UnitResult:displayFinalResult()
    if 0 == self.testCount then
      failurePercent = 0
      _G.print('Nothing to test')
    else
      self:displayFailedTests()
      local failurePercent, successCount
      failurePercent = 100 * self.failureCount / self.testCount
      successCount = self.testCount - self.failureCount
	    local s = _G.string.format('Success: %d%% - %d of %d',
        _G.math.ceil(100 * successCount / self.testCount), successCount, self.testCount)
	    -- _G.print(_G.string.rep ('=', _G.string.len(s)))
      _G.print(s)
    end
  end

  function UnitResult:startClass(className)
    self.currentClassName = className
    self:displayClassName()
  end

  function UnitResult:startTest(testName)
    self.currentTestName = testName
    self:displayTestName()
    self.testCount = self.testCount + 1
    self.testHasFailure = false
  end

  function UnitResult:addFailure(errorMsg)
    self.failureCount = self.failureCount + 1
    self.testHasFailure = true
    _G.table.insert(self.errorList, {self.currentTestName, errorMsg})
    --self:displayFailure(errorMsg)
  end

  function UnitResult:endTest()
    if not self.testHasFailure then
      self:displaySuccess()
    end
  end

  -- class UnitResult end
  
  -- class LuaUnit Begin
  
  LuaUnit = {result = UnitResult}
  
  -- Split text into a list consisting of the strings in text,
  -- separated by strings matching delimiter (which may be a pattern). 
  -- example: strsplit(',%s*', 'Anna, Bob, Charlie,Dolores')
  function LuaUnit.strsplit(delimiter, text)
    local list = {}
    local pos = 1
    if _G.string.find('', delimiter, 1) then -- this would result in endless loops
      _G.error('delimiter matches empty string!')
    end
    while 1 do
      local first, last = _G.string.find(text, delimiter, pos)
      if first then -- found?
        _G.table.insert(list, _G.string.sub(text, pos, first-1))
        pos = last+1
      else
        _G.table.insert(list, _G.string.sub(text, pos))
        break
      end
    end
    return list
  end

  function LuaUnit.isTable(aObject) 
    return 'table' == _G.type(aObject)
  end

  function LuaUnit.isFunction(aObject) 
    return 'function' == _G.type(aObject)
  end

  function LuaUnit.strip_luaunit_stack(stack_trace)
    stack_list = LuaUnit.strsplit('\n', stack_trace)
    strip_end = nil
    for i = _G.table.getn(stack_list),1,-1 do
      -- a bit rude but it works !
      if _G.string.find(stack_list[i],'[C]: in function "xpcall"',0,true) then
        strip_end = i - 2
      end
    end
    if strip_end then
      _G.table.setn(stack_list, strip_end)
    end
    stack_trace = _G.table.concat(stack_list, '\n')
    return stack_trace
  end

  function LuaUnit:runTestMethodSingle(aName, aMethod)

    local ok, errorMsg
    -- example: runTestMethod('test1', Test1)
    LuaUnit.result:startTest(aName)

    local function err_handler(e)
      return e..'\n'.._G.debug.traceback()
    end

    -- run testMethod()
    ok, errorMsg = _G.xpcall(aMethod, err_handler)
    if not ok then
      errorMsg  = self.strip_luaunit_stack(errorMsg)
      LuaUnit.result:addFailure(errorMsg)
    end

    self.result:endTest()
  end

  function LuaUnit:runTestMethod(aName, aClassInstance, aMethod)
    local ok, errorMsg
    -- example: runTestMethod('TestToto:test1', TestToto, TestToto.testToto(self))
    LuaUnit.result:startTest(aName)

    -- run setUp first(if any)
    if self.isFunction(aClassInstance.setUp) then
        aClassInstance:setUp()
    end

    local function err_handler(e)
      return e..'\n'.._G.debug.traceback()
    end

    -- run testMethod()
    ok, errorMsg = _G.xpcall(aMethod, err_handler)
    if not ok then
      errorMsg  = self.strip_luaunit_stack(errorMsg)
      LuaUnit.result:addFailure(errorMsg)
    end

    -- lastly, run tearDown(if any)
    if self.isFunction(aClassInstance.tearDown) then
       aClassInstance:tearDown()
    end

    self.result:endTest()
    end

  function LuaUnit:runTestMethodName(methodName, classInstance)
    -- example: runTestMethodName('TestToto:testToto', TestToto)
    local methodInstance = _G.loadstring(methodName .. '()')
    LuaUnit:runTestMethod(methodName, classInstance, methodInstance)
  end

  function LuaUnit:runTestClassByName(aClassName)
    -- example: runTestClassByName('TestToto')
    local hasMethod, methodName, classInstance
    hasMethod = _G.string.find(aClassName, ':')
    if hasMethod then
      methodName = _G.string.sub(aClassName, hasMethod+1)
      aClassName = _G.string.sub(aClassName,1,hasMethod-1)
    end
    
    classInstance = _G[aClassName]
    if not classInstance then
      _G.error('No such class: '..aClassName)
    end

    LuaUnit.result:startClass(aClassName)

    if hasMethod then
      if not classInstance[methodName] then
        _G.error('No such method: '..methodName)
      end
      LuaUnit:runTestMethodName(aClassName..':'.. methodName, classInstance)
    else
      -- 1.02.2620: check if class or method
      if LuaUnit.isTable(classInstance) then
        -- run all test methods of the class
        LuaUnit.result:displayStatus('-- run all test methods of the class')
        for methodName, method in orderedPairs(classInstance) do
          --for methodName, method in classInstance do
          if LuaUnit.isFunction(method) and _G.string.sub(methodName, 1, 4) == 'test' then
            LuaUnit:runTestMethodName(aClassName..':'.. methodName, classInstance)
          end
        end
      -- 1.02.2620: check if method
      elseif LuaUnit.isFunction(classInstance) then
        self.result:displayStatus('-- run test method')
        LuaUnit:runTestMethodSingle(aClassName, classInstance) 
      else
        _G.error('No such method: '..aClassName)
      end
    end
    _G.print()
  end

  function LuaUnit:run(...)
    -- Run some specific test classes.
    -- If no arguments are passed, run the class names specified on the
    -- command line. If no class name is specified on the command line
    -- run all classes whose name starts with 'Test'
    --
    -- If arguments are passed, they must be strings of the class names 
    -- that you want to run
    -- 1.02.2620: clear result
    self.result:clear()
    if arg.n > 0 then
      self.result:displayStatus('Runing "arg" tests: '.._G.unpack(arg))
      -- _G.table.foreachi(arg, LuaUnit.runTestClassByName)
			for i, v in _G.ipairs(arg) do 
				LuaUnit.runTestClassByName (i, v)
			end
			      
    else 
      if argv and argv.n > 0 then

        self.result:displayStatus('Runing "argv" tests: '.._G.unpack(arg))

        -- _G.table.foreachi(argv, LuaUnit.runTestClassByName)
        for i, v in _G.ipairs(argv) do 
					LuaUnit.runTestClassByName (i, v)
				end
        
      else
        self.result:displayStatus('Looking for Tests')
        -- create the list before. If you do not do it now, you
        -- get undefined result because you modify _G while iterating
        -- over it.
        testClassList = {}
        for key, val in _G do 
          if _G.string.sub(key,1,4) == 'Test' then 
            _G.table.insert(testClassList, key)
          end
        end
        self.result:displayStatus('-- run all test names')
        for i, val in orderedPairs(testClassList) do 
            LuaUnit:runTestClassByName(val)
        end
      end
    end
    self.result:displayFinalResult()
  end  
  
  -- class LuaUnit

  -- return was commented out and the reset of the environment was added to fix
  -- the problem of packages not loading in a configuration sequence.
  _G.setfenv(1, _G)
  --  return Public

  -- Store chunk name at a standard location for require() to look it up.
  _G._LOADED[_chunkName]=true

end
