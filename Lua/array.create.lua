--- <summary> Array creation functions. </summary>
--- <remarks> The array functions were written so as to work with TSP buffers. </remarks>
--- <namespace> isr.array </namespace>
--- <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
--- Licensed under The MIT License. </para><para>
--- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
--- BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
--- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--- </para> </license>
--- <history date='03/24/05' by='David Hary' revision='1.0.1909.x'> Created </history>

do

  local _chunkName = 'isr.array.create.lua'

  -- aliases
  local _require = require 'require.lua'
  _require('isr.array.lua', _chunkName)

  --- <summary> Defines the ISR name space. </summary>
  isr = isr or {}

  --- <summary> Defines the ISR array name space. </summary>
  isr.array = isr.array or {}

  local Public = isr.array
  -- Get local access to the global environment
  local _G = _G
  -- Declare all public functions as global variables and they will go to 
  -- a separate table automatically. Register this table as the package name. 
  setfenv(1, Public)

  --- <summary> Creates a ramp array . </summary>
  --- <param name='initial'> The initial value. </param>
  --- <param name='increment'> The increment. </param>
  --- <param name='size'> The array size. </param>
  --- <example> <para>
  --- a = isr.array.createLinearArray(1, 1, 3)
  ---                                   </para><para> 
  --- isr.array.listValues(a, '%d')
  ---                                   </para><para> 
  --- </para> </example>
  --- <returns> Array. </returns> 
  function createLinearArray(initial, increment, size)

    local newArray = {}
    setn(newArray, size)
    for item = 1, size do
      newArray[item] = initial + increment * (item - 1)
    end

    return newArray

  end

  --- <summary> Creates a random array with range [low, high). </summary>
  --- <param name='size'> The array size. </param>
  --- <param name='low'> The low limit. </param>
  --- <param name='high'> The high limit. </param>
  --- <example> <para>
  --- a = isr.array.createRandomArray(3, 0, 10)
  ---                                   </para><para> 
  --- isr.array.listValues(a, '%7.3f')
  ---                                   </para><para> 
  --- </para> </example>
  --- <returns> Array. </returns> 
  function createRandomArray(size, low, high)

    local newArray = {}
    setn(newArray, size)
    local range = high - low
    local r = _G.math.random
    for k = 1, size do
      newArray[k] = low + range * r()
    end

    return newArray

  end

  --- <summary> Creates a random integer array with range [low, high]. </summary>
  --- <param name='size'> The array size. </param>
  --- <param name='low'> The low limit. </param>
  --- <param name='high'> The high limit. </param>
  --- <example> <para>
  --- a = isr.array.createRandomIntegerArray(3, 0, 100)
  ---                                   </para><para> 
  --- isr.array.listValues(a, '%f')
  ---                                   </para><para> 
  --- </para> </example>
  --- <returns> Array. </returns> 
  function createRandomIntegerArray(size, low, high)

    local newArray = {}
    setn(newArray, size)
    local r = _G.math.random
    for k = 1, size do
      newArray[k] = r(low, high)
    end

    return newArray

  end

  -- return was commented out and the reset of the environment was added to fix
  -- the problem of packages not loading in a configuration sequence.
  _G.setfenv(1, _G)

  -- Store chunk name at a standard location for require() to look it up.
  _G._LOADED[_chunkName] = isr.array

end
