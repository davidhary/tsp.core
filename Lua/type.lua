--- <summary> Type identification functions. </summary>
--- <namespace> _G.type </namespace>
--- <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
--- Licensed under The MIT License. </para><para>
--- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
--- BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
--- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--- </para> </license>
--- <history date='03/11/09' by='David Hary' revision='2.1.3357.x'> Add non-nil True and 
--- False values compatible with Visual Studio formatting. </history>
--- <history date='09/09/09' by='David Hary' revision='2.1.3539.x'> Fix XML documentation. </history>
--- <history date='03/08/07' by='David Hary' revision='1.0.2623.x'> Created </history>

do

  local _chunkName = 'g.type.lua'

  --- <summary> Defines a boolean True value compatible with Visual Studio formatting. </summary>
  True = true
  
  --- <summary> Defines a boolean False value compatible with Visual Studio formatting. </summary>
  False = false

  --- <summary> Checks if the object is a <c>boolean</c>. </summary>
  --- <param name='value'> A reference to an object. </param>
  --- <returns> <c>true</c> if the object is a <c>boolean</c>. </returns>
  isBoolean = function(value) 
    return 'boolean' == type(value)
  end

  --- <summary> Checks if the object is <c>function</c>. </summary>
  --- <param name='value'> A reference to an object. </param>
  --- <returns> <c>true</c> if the object is <c>function</c>. </returns>
  isFunction = function(value) 
    return 'function' == type(value)
  end

  --- <summary> Checks if the object is <c>number</c>. </summary>
  --- <param name='value'> A reference to an object. </param>
  --- <returns> <c>true</c> if the object is <c>number</c>. </returns>
  isNumber = function(value) 
    return 'number' == type(value)
  end

  --- <summary> Checks if the object is <c>string</c>. </summary>
  --- <param name='value'> A reference to an object. </param>
  --- <returns> <c>true</c> if the object is <c>string</c>. </returns>
  isString = function(value) 
    return 'string' == type(value)
  end

  --- <summary> Checks if the object is <c>table</c>. </summary>
  --- <param name='value'> A reference to an object. </param>
  --- <returns> <c>true</c> if the object is <c>table</c>. </returns>
  isTable = function(value) 
    return 'table' == type(value)
  end

  --- <summary> Checks if the object is <c>thread</c>. </summary>
  --- <param name='value'> A reference to an object. </param>
  --- <returns> <c>true</c> if the object is <c>thread</c>. </returns>
  isThread = function(value) 
    return 'thread' == type(value)
  end

  --- <summary> Checks if the object is user data. </summary>
  --- <param name='value'> A reference to an object. </param>
  --- <returns> <c>true</c> if the object is <c>userdata</c>. </returns>
  isUserdata = function(value) 
    return 'userdata' == type(value)
  end

  -- Store chunk name at a standard location for require() to look it up.
  _LOADED[_chunkName] = true

end
