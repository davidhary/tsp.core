--- <summary> Includes array functions. </summary>
--- <remarks> Array bounds are used to allow working with TSP buffers. </remarks>
--- <namespace> isr.array </namespace>
--- <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
--- Licensed under The MIT License. </para><para>
--- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
--- BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
--- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--- </para> </license>
--- <history date='03/24/05' by='David Hary' revision='1.0.1909.x'> Created </history>

do

  local _chunkName = 'isr.array.lua'

  -- aliases
  local _require = require 'require.lua'
  local _table = _require('read.only.table.lua', _chunkName)

  --- <summary> Defines the ISR name space. </summary>
  isr = isr or {}

  --- <summary> Defines the ISR array name space as a copy of the 'table'. </summary>
  isr.array = _table.copy(table)

  local Public = isr.array
  -- Get local access to the global environment
  local _G = _G
  -- Declare all public functions as global variables and they will go to 
  -- a separate table automatically. Register this table as the package name. 
  setfenv(1, Public)

  --- <summary> Prints the array values for each element. </summary>
  --- <param name='a'> The array. </param>
  --- <param name='format'> The print format ('%d, %f'). </param>
  --- <param name='header'> The print header (nil). </param>
  --- <remarks> Does not work with TSP buffers. </remarks>
  --- <example> <para>
  --- a = {1,b=2,3,c=4}
  ---                                   </para><para> 
  --- isr.array.listValues(a, '%d')
  ---                                   </para><para> 
  --- </para> </example>
  listValues = function(a, format, header)

    if nil ~= header then
      _G.print(header)
    end

    if nil == format then
      format = '%f' 
    end
    for i, v in _G.ipairs(a) do
      _G.print(_G.string.format(format, v))
    end

  end
  
  --- <summary> Prints the array index and value for each element. </summary>
  --- <param name='a'> The array. </param>
  --- <param name='format'> The print format ('%d, %f'). </param>
  --- <param name='header'> The print header (nil). </param>
  --- <remarks> Does not work with TSP buffers. </remarks>
  --- <example> <para>
  --- a = {1,b=2,3,c=4}
  ---                                   </para><para> 
  --- isr.array.listIndexValues(a, '%d, %d')
  ---                                   </para><para> 
  --- </para> </example>
  listIndexValues = function(a, format, header)

    if nil ~= header then
      _G.print(header)
    end

    if nil == format then
      format = '%d, %f' 
    end
    for i, v in _G.ipairs(a) do
      _G.print(_G.string.format(format, i, v))
    end

  end
  
  --- <summary> Returns a new array that is the concatenation of two arrays. </summary>
  --- <param name='a'> The first array. </param>
  --- <param name='b'> The second array. </param>
  --- <param name='from'> The starting index (1). </param>
  --- <param name='to'> The ending index (length). </param>
  --- <remarks> Not compatible with TSP buffers. </remarks>
  --- <example> <para>
  --- a = {1,b=2,3,c=4}
  --- b = {11,b=12,13,c=14}
  ---                                   </para><para> 
  --- isr.array.listIndexValues(isr.array.concatenate(a,b),'x[%d]=%2d')
  --- x[1]= 1
  --- x[2]= 3
  --- x[3]=11
  --- x[4]=13
  ---                                   </para><para> 
  --- </para> </example>
  --- <returns> Array. </returns> 
  concatenate = function(a, b)

    local i, j = 1, getn(a)
    local k, l = 1, getn(b)

    local newArray = {}
    setn(newArray, j - i + 1 + l - k + 1)
    local p = 0
    for q = i, j  do
      p = p + 1
      newArray[p] = a[q] 
    end
    for q = k, l  do
      p = p + 1
      newArray[p] = b[q]
    end

    return newArray

  end

  --- <summary> Creates a copy of the array. </summary>
  --- <param name='a'> The array. </param>
  --- <param name='from'> The starting index (1). </param>
  --- <param name='to'> The ending index (length). </param>
  --- <example> <para>
  --- a = {1,b=2,3,c=4}
  ---                                   </para><para>
  --- print(3 or 1, 6 or isr.array.getn(a)) 
  ---                                   </para><para>
  --- b = _G.isr.array.copy(a)
  ---                                   </para><para> 
  --- isr.array.listIndexValues(isr.array.concatenate(a,b),'x[%d]=%2d')
  --- x[1]= 1
  --- x[2]= 3
  --- x[3]= 1
  --- x[4]= 3
  ---                                   </para><para> 
  --- </para> </example>
  --- <remarks> the 'from' and 'to' arguments must be provided for TSP buffers. </remarks>
  --- <returns> Array. </returns> 
  copy = function(a, from, to)

    local i, j = from or 1, to or getn(a)

    local newArray = {}
    setn(newArray, j - i + 1)
    for k = i, j  do
      newArray[k - i + 1] = a[k]
    end

    return newArray

  end

  --- <summary> Looks for the first index where a[i] exceeds or is lower than the 
  --- limen for the specific duration and hysteresis. </summary>
  --- <param name='a'> The array. </param>
  --- <param name='limen'> The limen value. </param>
  --- <param name='hysteresis'> The hysteresis value.  Use negative hysteresis for 
  ---  detecting offset. </param>
  --- <param name='minimumCount'> The minimum number of indexes during which the 
  --- signal must be on or off. </param>
  --- <param name='from'> The starting index (1). </param>
  --- <param name='to'> The ending index (length). </param>
  --- <remarks> the 'from' and 'to' arguments must be provided for
  ---   TSP buffers. </remarks>
  --- <example> <para>
  --- a = {1,b=2,3,c=4}
  ---                                   </para><para>
  --- print(3 or 1, 6 or isr.array.getn(a)) 
  ---                                   </para><para>
  --- b = _G.isr.array.copy(a)
  ---                                   </para><para> 
  --- isr.array.listIndexValues(isr.array.concatenate(a,b),'x[%d]=%2d')
  --- x[1]= 1
  --- x[2]= 3
  --- x[3]= 1
  --- x[4]= 3
  ---                                   </para><para> 
  --- </para> </example>
  --- <returns> onset, offset; indexes. </returns> 
  findLimen = function(a, limen, hysteresis, minimumCount, from, to)

    local onset, j = from or 1, to or getn(a)
    local offset = 0
    
    repeat 
      if hysteresis > 0 then
        onset = findOnset(a, limen, onset, j)
        if onset > 0 then
          offset = findOffset(a, limen - hysteresis, onset, j)
        end
      elseif 0 == hysteresis then
        onset = findOnset(a, limen, onset, j)
        offset = onset + minimumCount
      else
        onset = findOffset(a, limen, onset, j)
        if onset > 0 then
          offset = findOnset(a, limen - hysteresis, onset, j)
        end
      end
      
    until 0 == onset or 0 == offset or ((offset - onset) >= minimumCount)
    
    return onset, offset

  end

  --- <summary> Looks for the first index where a[i] is lower than 
  --- limen or zero if the offset did not occur. </summary>
  --- <param name='a'> The array. </param>
  --- <param name='limen'> The limen value. </param>
  --- <param name='from'> The starting index (1). </param>
  --- <param name='to'> The ending index (length). </param>
  --- <remarks> the 'from' and 'to' arguments must be provided for
  ---   TSP buffers.
  --- Tested 08/02/2008. </remarks>
  --- <returns> offset index. </returns> 
  findOffset = function(a, limen, from, to)

    local i, j = from or 1, to or getn(a)

    local offset = 0
    while 0 == offset and i < j do
      if a[i] < limen then
        offset = i
      end
      i = i + 1
    end

    return offset

  end

  --- <summary> Looks for the first index where a[i] > limen or zero if the onset did 
  ---   not occur. </summary>
  --- <param name='a'> The array. </param>
  --- <param name='limen'> The limen value. </param>
  --- <param name='from'> The starting index (1). </param>
  --- <param name='to'> The ending index (length). </param>
  --- <remarks> the 'from' and 'to' arguments must be provided for
  ---   TSP buffers.
  --- Tested 08/02/2008. </remarks>
  --- <returns> onset index. </returns> 
  findOnset = function(a, limen, from, to)

    local i, j = from or 1, to or getn(a)

    local onset = 0
    while 0 == onset and i < j do
      if a[i] > limen then
        onset = i
      end
      i = i + 1
    end

    return onset

  end

  --- <summary> Creates an a array of the function values of the
  ---   input array elements, e.g., newArray = f(oldArray). </summary>
  --- <param name='a'> The array. </param>
  --- <param name='f'> The function to use on array elements. </param>
  --- <param name='from'> The starting index (1). </param>
  --- <param name='to'> The ending index (length). </param>
  --- <remarks> the 'from' and 'to' arguments must be provided for
  ---   TSP buffers. </remarks>
  --- <returns> Array. </returns> 
  functionArray = function(a, f, from, to)

    local i, j = from or 1, to or getn(a)

    local newArray = {}
    setn(newArray, j - i + 1)
    for k = i, j  do
      newArray[k - i + 1] = f(a[k])
    end

    return newArray

  end
  
  --- <summary> Shifts the array by the specified amount. </summary>
  --- <param name='a'> The array. </param>
  --- <param name='n'> The shift amount. </param>
  --- <param name='from'> The starting index (1). </param>
  --- <param name='to'> The ending index (length). </param>
  --- <remarks> The 'from' and 'to' arguments must be provided for
  ---   TSP buffers.
  --- Tested 08/02/2008. </remarks>
  --- <returns> Array. </returns> 
  shift = function(a, c, from, to)

    local i, j = from or 1, to or getn(a)

    local newArray = {}
    setn(newArray, j - i + 1)
    if c > 0 then
      for k = j, i + c, -1 do
        newArray[k] = a[k - c]
      end
      for k = i, i + c - 1 do
        newArray[k] = 0
      end
    elseif 0 > c then
      for k = i, j + c do
        newArray[k] = a[k - c]
      end
      for k = j + c + 1, j do
        newArray[k] = 0
      end
    end

    return newArray

  end

  -- return was commented out and the reset of the environment was added to fix
  -- the problem of packages not loading in a configuration sequence.
  _G.setfenv(1, _G)

  -- Store chunk name at a standard location for require() to look it up.
  _G._LOADED[_chunkName] = isr.array

end
