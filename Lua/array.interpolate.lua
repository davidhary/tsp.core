--- <summary> Linear interpolation array functions. </summary>
--- <remarks> The array functions were written so as to work with TSP buffers. </remarks>
--- <namespace> isr.array </namespace>
--- <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
--- Licensed under The MIT License. </para><para>
--- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
--- BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
--- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--- </para> </license>
--- <history date='03/24/05' by='David Hary' revision='1.0.1909.x'> Created </history>

do

  local _chunkName = 'isr.array.interpolate.lua'

  -- aliases
  local _require = require 'require.lua'
  _require('isr.array.lua', _chunkName)
  _require('isr.array.algebra.lua', _chunkName)

  --- <summary> Defines the ISR name space. </summary>
  isr = isr or {}

  --- <summary> Defines the ISR array name space. </summary>
  isr.array = isr.array or {}

  local Public = isr.array
  -- Get local access to the global environment
  local _G = _G
  -- Declare all public functions as global variables and they will go to 
  -- a separate table automatically. Register this table as the package name. 
  setfenv(1, Public)

  --- <summary> Calculates the offset (y at x=0) and slope (dy/dx) of y over x. </summary>
  --- <param name='x'> The abscissa. </param>
  --- <param name='y'> The ordinate. </param>
  --- <param name='from'> The starting index (1). </param>
  --- <param name='to'> The ending index (length). </param>
  --- <returns> offset, slope pair. </returns>
  --- <example> <para>
  --- local offset, slope = isr.array.linearFit(xArray, yArray)
  --- a = isr.array.createLinearArray(1, 1, 5)
  --- offset, slope = isr.array.linearFit(a, isr.array.linearScale(a, 2, 0.5))
  --- print(string.format(' offset: %f, slope: %f', offset, slope)) -- .5, 2
  --- </para> </example>
  --- <remarks> The 'from' and 'to' arguments must be provided for
  --- TSP buffers.
  --- Tested 08/02/2008. </remarks>
  --- <returns> Offset, slope. </returns> 
  function linearFit(x, y, from, to)

    local i, j = from or 1, to or getn(x)

    local meanX = sum(x, i, j) / (j - i + 1)
    local meanY = sum(y, i, j) / (j - i + 1)
    local xm = linearScale(x, 1, -meanX, i, j)
    local ym = linearScale(y, 1, -meanY, i, j)
    local sumXX = sum(product(xm, xm))
    local sumXY = sum(product(xm, ym))

    if sumXX > 0 then

      -- Compute the Slope
      slope = sumXY / sumXX

      -- Compute the offset
      offset = meanY - meanX * slope

    else

      -- If no X deviations, we cannot computer the slope
      slope = 0
      offset = 0

    end

    return offset, slope

  end

  --- <summary> Applies linear interpolation to yield y(x). </summary>
  --- <param name='x'> The abscissa value to interpolate to an ordinate value </param>
  --- <param name='x1'> The first abscissa value. </param>
  --- <param name='x2'> The second abscissa value. </param>
  --- <param name='y1'> The first ordinate value. </param>
  --- <param name='y2'> The second ordinate value. </param>
  --- <returns> The ordinate value derived using linear interpolation based on a pair of 
  ---   coordinates. </returns>
  --- <remarks> Tested 08/02/2008. </remarks>
  --- <returns> y(x). </returns> 
  function linearInterpolatePair(x, x1, x2, y1, y2)

    -- check if we have two distinct pairs
    if x2 == x1 then
      -- if we have identical X values, return the
      -- average of the ordinate value
      return 0.5 * (y1 + y2)
    else
      return y1 + (x - x1) * (y2 - y1) / (x2 - x1)
    end

  end 

  --- <summary> Returns an array which values are interpolated 
  ---   based on the old array values. </summary>
  --- <param name='newX'> The array of new abscissa values </param>
  --- <param name='oldX'> The array of abscissa values. </param>
  --- <param name='oldY'> The array of ordinate values. </param>
  --- <param name='from'> The starting index (1). </param>
  --- <param name='to'> The ending index (length). </param>
  --- <returns> The new array of ordinate values. </returns>
  --- <remarks> The 'from' and 'to' arguments must be provided for
  ---   TSP buffers.
  --- Tested 08/02/2008. </remarks>
  --- <example> <para>
  ---                                   </para><para>
  --- a = isr.array.createLinearArray(1, 1, 5)
  --- b = isr.array.createLinearArray(0, 0.5, 5)
  ---                                   </para><para> 
  --- isr.array.printTwoArrays(b, isr.array.linearInterpolate(b, a, a))
  ---                                   </para><para> 
  --- </para> </example>
  --- <returns> Array. </returns> 
  function linearInterpolate(newX, oldX, oldY, from, to)

    local i, j = from or 1, to or getn(newX) 

    local n = j - i + 1
    local newArray = {}
    setn(newArray, n)
    local p, q = i, i + 1
    x1 = oldX[p]
    x2 = oldX[q]
    for k = 1, n do
      -- lookup the index where x[p] <= newX <= x[q]
      -- or newX <= oldX[1] or  oldX[n] <= newX
      x = newX[k]
      while not((x <= x1 and p == i) or (x >= x1 and x <= x2) or (x >= x2 and q == j)) do
        p = q
        q = q + 1
        x1 = oldX[p]
        x2 = oldX[q]
      end
      -- printf('k: %d, p: %d, q: %d, x: %f', k, p, q, x) 
      -- printf('x1: %f, x2: %f, y1: %f, y2: %f', x1, x2, oldY[p], oldY[q]) 
      newArray[k] = linearInterpolatePair(x, x1, x2, oldY[p], oldY[q])
    end

    return newArray

  end 

  --- <summary> Scales an array y = a*x + b. </summary>
  --- <param name='x'> The array to scale. </param>
  --- <param name='a'> The slope. </param>
  --- <param name='b'> The offset. </param>
  --- <param name='from'> The starting index (1). </param>
  --- <param name='to'> The ending index (length). </param>
  --- <remarks> The 'from' and 'to' arguments must be provided for
  ---   TSP buffers.
  --- Tested 08/02/2008. </remarks>
  --- <returns> Array. </returns> 
  function linearScale(x, a, b, from, to)

    local i, j = from or 1, to or getn(x)

    local newArray = {}
    if a == 1 then
      if 0 == b then
        newArray = copy(x, i, j)
      else
        for k = i, j  do
          newArray[k - i + 1] = x[k] + b
        end
      end
    else
      if 0 == b then
        for k = i, j  do
          newArray[k - i + 1] = a * x[k]
        end
      else
        for k = i, j  do
          newArray[k - i + 1] = a * x[k] + b
        end
      end
    end

    return newArray

  end

  -- return was commented out and the reset of the environment was added to fix
  -- the problem of packages not loading in a configuration sequence.
  _G.setfenv(1, _G)

  -- Store chunk name at a standard location for require() to look it up.
  _G._LOADED[_chunkName] = isr.array

end
