--- <namespace> _G </namespace>
--- <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
--- Licensed under The MIT License. </para><para>
--- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
--- BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
--- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--- </para> </license>
--- <history date='03/28/07' by='David Hary' revision='1.0.2643.x'> Created </history>

do

  local _chunkName = 'g.benchmark.lua'

  require('g.require.lua', _chunkName)
  require('g.printf.lua', _chunkName)

  --- <summary> times the specified function. </summary>
  --- <param name='f'> References a function. </param>
  --- <param name='...'> Function arguments. </param>
  --- <example> <para>
  --- benchmark('F ... time: ', f, 1, 2).
  --- benchmark('getmetatable(node) ... time: ', getmetatable, node)
  --- </para> </example>
  function benchmark(message, f, ...)
    if nil == message then
      message = 'Execution time = '
    end 
    local startTime = _G.os.clock()
    local ok, error_msg = _G.pcall(f, _G.unpack(arg))
    if not ok then 
      if nil == error_msg then
        print ('Unknown benchmarked function error/bad function')
      else
        print ('Benchmarked function error: ' .. error_msg)
      end
    else
      local endTime = _G.os.clock()
      -- does not work - gives a second resolution
      -- printf('%s %f ', message, _G.os.difftime(endTime - startTime))
      printf('%s %f ', message, endTime - startTime)
    end 
  end

  -- Store chunk name at a standard location for require() to look it up.
  _G._LOADED[_chunkName]=true

end
