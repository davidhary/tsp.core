# Test Script Processing and Lua Core Project

A library for support and extension of the [LUA] (https://www.lua.org) and Test Script for use in the TSP [Keithley] (https://www.keithley.com) instruments.

## Getting Started

Clone the project along with its requisite projects to their respective relative path. All projects are installed relative to a common path.

### Prerequisites

The following projects are also required:
* [TSP Core](https://www.bitbucket.org/davidhary/tsp.core) - Test Script Processing and Lua Core Project

```
git clone git@bitbucket.org:davidhary/tsp.core.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
.\Libraries\TSP\Core
```

## Testing

The project includes a few unit test classes. Test applications are under the *Apps* solution folder. 

## Deployment

Deployment projects have not been created for this project.

## Built, Tested and Facilitated By

* [Test Script Bulder](https://www.keithley.com) - TYest Script Builder
* [Jarte](https://www.jarte.com/) - RTF Editor

## Authors

* **David Hary** - *Initial Workarounds* - [ATE Coder](https://www.IntegratedScientificResources.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md] file at (https://www.bitbucket.org/davidhary/tsp.core/src) for details

## Acknowledgments

* Hat tip to anyone who's code was used
* [Its all a remix] (www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [Stack overflow] (https://www.stackoveflow.com)
* [LUA] (https://www.lua.org) -- The lua programming language
* [Keithley] (https://www.keithley.com) - Test Script builder
* [Table Copy] (\fldinst{HYPERLINK "http://www.loop.org"} -- Table Copy
* [Lua Base 64 Coder] (http://www.it-rfc.de) -- Working LUA base-64 codec

## Revision Changes

* Version 4.0.5242	05/09/14	Adds support for TSP 2.0. Adds display text and input. Adds device with IEEE 488 instrument properties. 
* Version 4.0.5237	05/04/14	Upgrades to using extended string, user string, array, table, math and type functions.
* Version 4.0.5232	04/30/14	Removes Commit history. Uses MIT license. Adds spaces to comments. Removes new lines in comments. 
Created based on the legacy support project aiming to reduce the footprint of the TTM package by removing the KTS code.
